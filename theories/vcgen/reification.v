From iris_c.vcgen Require Export dcexpr.

(** * Reification of C syntax *)
(** ** IntoDBool *)
Class IntoDBool (b : bool) (db : dbool) :=
  into_dbool : b = dbool_interp db.

Instance into_dbool_true : IntoDBool true (dBoolKnown true).
Proof. done. Qed.
Instance into_dbool_false : IntoDBool false (dBoolKnown false).
Proof. done. Qed.
Instance into_dbool_unknown b : IntoDBool b (dBoolUnknown b) | 100.
Proof. done. Qed.

(** ** IntoDInt *)
Class NatIntoDInt (n : nat) (di : dint) :=
  nat_into_dint : Z.of_nat n = dint_interp di.

Instance nat_into_dint_0 : NatIntoDInt 0 (dInt 0 None).
Proof. done. Qed.
Instance nat_into_dint_S n k u :
  NatIntoDInt n (dInt k u) → NatIntoDInt (S n) (dInt (Z.succ k) u).
Proof. rewrite /NatIntoDInt !dint_interp_alt /=. lia. Qed.
Instance nat_into_dint_plus n1 n2 di1 di2 :
  NatIntoDInt n1 di1 → NatIntoDInt n2 di2 →
  NatIntoDInt (n1 + n2) (dint_plus di1 di2).
Proof. rewrite /NatIntoDInt dint_plus_correct. lia. Qed.
Instance nat_into_dint_default n : NatIntoDInt n (dInt 0 (Some (IntNat n))) | 100.
Proof. done. Qed.

Class ZIntoDInt (i : Z) (di : dint) :=
  Z_into_dint : i = dint_interp di.

Instance Z_into_dint_nat n di : NatIntoDInt n di → ZIntoDInt (Z.of_nat n) di | 0.
Proof. done. Qed.
Instance Z_into_dint_0 : ZIntoDInt 0 (dInt 0 None).
Proof. done. Qed.
(* TODO: check if p is ground *)
Instance Z_into_dint_Zpos p : ZIntoDInt (Zpos p) (dInt (Zpos p) None) | 10.
Proof. by rewrite /ZIntoDInt dint_interp_alt. Qed.
Instance Z_into_dint_Zneg p : ZIntoDInt (Zneg p) (dInt (Zneg p) None) | 10.
Proof. done. Qed.
Instance Z_into_dint_S i k u :
  ZIntoDInt i (dInt k u) → ZIntoDInt (Z.succ i) (dInt (Z.succ k) u).
Proof. rewrite /ZIntoDInt !dint_interp_alt /=. lia. Qed.
Instance Z_into_dint_plus i1 i2 di1 di2 :
  ZIntoDInt i1 di1 → ZIntoDInt i2 di2 → ZIntoDInt (i1 + i2) (dint_plus di1 di2).
Proof. rewrite /ZIntoDInt dint_plus_correct. lia. Qed.
Instance Z_into_dint_default i : ZIntoDInt i (dInt 0 (Some (IntZ i))) | 100.
Proof. done. Qed.

(** ** LocLookup *)
Class LocLookup (E E' : known_locs) (cl : cloc) (i : nat) := {
  loc_lookup : E' !! i = Some cl;
  loc_lookup_mono : E `prefix_of` E';
}.

Instance loc_lookup_new cl : LocLookup [] [cl] cl 0.
Proof. split. done. by exists [cl]. Qed.
Instance loc_lookup_here cl E : LocLookup (cl :: E) (cl :: E) cl 0 | 0.
Proof. done. Qed.
Instance loc_lookup_there cl cl' E E' i :
  LocLookup E E' cl i → LocLookup (cl' :: E) (cl' :: E') cl (S i) | 1.
Proof. intros [??]; split; auto using prefix_cons. Qed.

(* IntoDLoc *)
Class IntoDLoc (E E' : known_locs) (cl : cloc) (dl : dloc) := {
  into_dloc : cl = dloc_interp E' dl;
  into_dloc_wf : dloc_wf E' dl;
  into_dloc_mono : E `prefix_of` E';
}.

Instance into_dloc_lookup E E' cl i :
  LocLookup E E' cl i → IntoDLoc E E' cl (dLoc i (dInt 0 None)) | 10.
Proof.
  intros [Hi ?]. split=> //=.
  - by rewrite /dloc_var_interp Hi.
  - by rewrite /dloc_wf /= /dloc_var_wf Hi.
Qed.
Instance into_dloc_plus E E' cl dl i di :
  IntoDLoc E E' cl dl → ZIntoDInt i di →
  IntoDLoc E E' (cl +∗ i) (dloc_plus dl di) | 1.
Proof.
  rewrite /ZIntoDInt=> -[-> ??] ->. split=> //=.
  - by rewrite dloc_plus_correct.
  - by apply dloc_plus_wf.
Qed.

(** ** IntoDBaseLit *)
Class IntoDBaseLit (l : base_lit) (dl : dbase_lit) :=
  into_dbase_lit : l = dbase_lit_interp dl.

Instance into_dbase_lit_int i di :
  ZIntoDInt i di → IntoDBaseLit (LitInt i) (dLitInt di).
Proof. by rewrite /ZIntoDInt=>->. Qed.
Instance into_dbase_lit_bool b db :
 IntoDBool b db → IntoDBaseLit (LitBool b) (dLitBool db).
Proof. by intros ->. Qed.
Instance into_dbase_lit_unit : IntoDBaseLit LitUnit dLitUnit.
Proof. done. Qed.
Instance into_dbase_lit_default l : IntoDBaseLit l (dLitUnknown l) | 100.
Proof. done. Qed.

(** ** IntoDVal *)
Class IntoDVal (E E' : known_locs) (v : val) (dv : dval) := {
  into_dval : v = dval_interp E' dv;
  into_dval_wf : dval_wf E' dv;
  into_dval_mono : E `prefix_of` E';
}.

Instance into_dval_base_lit E l dl :
  IntoDBaseLit l dl → IntoDVal E E (LitV l) (dLitV dl).
Proof. by rewrite /IntoDBaseLit=> ->. Qed.
Instance into_dval_pair E E' E'' v1 v2 dv1 dv2 :
  IntoDVal E E' v1 dv1 → IntoDVal E' E'' v2 dv2 →
  IntoDVal E E'' (PairV v1 v2) (dPairV dv1 dv2).
Proof.
  intros [-> ??] [-> ??]; split=> /=; last by etrans.
  - f_equal. by apply dval_interp_mono.
  - naive_solver eauto using dval_wf_mono.
Qed.
Instance into_dval_loc E E' cl dl :
  IntoDLoc E E' cl dl → IntoDVal E E' (cloc_to_val cl) (dLocV dl).
Proof. intros [-> ??]; by split. Qed.
Instance into_dval_none E : IntoDVal E E NONEV dNone.
Proof. done. Qed.
Instance into_dval_default E v : IntoDVal E E v (dValUnknown v) | 100.
Proof. done. Qed.

(** ** IntoDExpr *)
Class IntoDExpr (E E' : known_locs) (e: expr) (de: dexpr) := {
  into_dexpr : e = dexpr_interp E' de;
  into_dexpr_wf : dexpr_wf E' de;
  into_dexpr_mono : E `prefix_of` E';
}.

Instance into_dexpr_val E E' v dv :
  IntoDVal E E' v dv → IntoDExpr E E' (Val v) (dEVal dv).
Proof. intros [-> ??]; by split. Qed.
Instance into_dexpr_var E x : IntoDExpr E E (Var x) (dEVar x).
Proof. done. Qed.
Instance into_dexpr_pair E E' E'' e1 e2 de1 de2 :
  IntoDExpr E E' e1 de1 → IntoDExpr E' E'' e2 de2 →
  IntoDExpr E E'' (Pair e1 e2) (dEPair de1 de2).
Proof.
  intros [-> ??] [-> ??]; split=> /=; last by etrans.
  - f_equal. by apply dexpr_interp_mono.
  - naive_solver eauto using dexpr_wf_mono.
Qed.
Instance into_dexpr_fst E E' e de:
  IntoDExpr E E' e de → IntoDExpr E E' (Fst e) (dEFst de).
Proof. intros [-> ??]; by split. Qed.
Instance into_dexpr_snd E E' e de:
  IntoDExpr E E' e de → IntoDExpr E E' (Snd e) (dESnd de).
Proof. intros [-> ??]; by split. Qed.
Instance into_dexpr_none E : IntoDExpr E E NONE dENone.
Proof. done. Qed.
Instance into_dexpr_unknown E e : IntoDExpr E E e (dEUnknown e) | 100.
Proof. done. Qed.

(** ** IntoDCExpr *)
Class IntoDCExpr (E E' : known_locs) (e: expr) (de: dcexpr) := {
  into_dcexpr : e = dcexpr_interp E' de;
  into_dcexpr_wf : dcexpr_wf E' de;
  into_dcexpr_mono : E `prefix_of` E';
}.

Local Ltac solve_into_dcexpr2 :=
  intros [-> ??] [-> ??]; split=> /=;
    [auto 10 using dcexpr_interp_mono with f_equal
    |naive_solver eauto 10 using dcexpr_wf_mono
    |by etrans].

Instance into_dcexpr_ret E E' e de :
  IntoDExpr E E' e de → IntoDCExpr E E' (c_ret e) (dCRet de).
Proof. intros [-> ??]; by split. Qed.
Instance into_dcexpr_seq_bind E E' E'' x e1 e2 de1 de2 :
  IntoDCExpr E E' e1 de1 → IntoDCExpr E' E'' e2 de2 →
  IntoDCExpr E E'' (x ←ᶜ e1 ;ᶜ e2) (dCSeqBind x de1 de2).
Proof. solve_into_dcexpr2. Qed.
Instance into_dcexpr_mut_bind E E' E'' x e1 e2 de1 de2 :
  IntoDCExpr E E' e1 de1 → IntoDCExpr E' E'' e2 de2 →
  IntoDCExpr E E'' (x ←mutᶜ e1 ;ᶜ e2) (dCMutBind x de1 de2).
Proof. solve_into_dcexpr2. Qed.
Instance into_dcexpr_alloc E E' E'' e1 e2 de1 de2 :
  IntoDCExpr E E' e1 de1 → IntoDCExpr E' E'' e2 de2 →
  IntoDCExpr E E'' (allocᶜ (e1, e2)) (dCAlloc de1 de2).
Proof. solve_into_dcexpr2. Qed.
Instance into_dcexpr_load E E' e de :
  IntoDCExpr E E' e de → IntoDCExpr E E' (c_load e) (dCLoad de).
Proof. intros [-> ??]; by split. Qed.
Instance into_dcexpr_store E E' E'' e1 e2 de1 de2 :
  IntoDCExpr E E' e1 de1 → IntoDCExpr E' E'' e2 de2 →
  IntoDCExpr E E'' (c_store e1 e2) (dCStore de1 de2).
Proof. solve_into_dcexpr2. Qed.
Instance into_dcexpr_binop E E' E'' e1 e2 op de1 de2 :
  IntoDCExpr E E' e1 de1 → IntoDCExpr E' E'' e2 de2 →
  IntoDCExpr E E'' (c_bin_op op e1 e2) (dCBinOp op de1 de2).
Proof. solve_into_dcexpr2. Qed.
Instance into_dcexpr_prebinop E E' E'' e1 e2 op de1 de2 :
  IntoDCExpr E E' e1 de1 → IntoDCExpr E' E'' e2 de2 →
  IntoDCExpr E E'' (c_pre_bin_op op e1 e2) (dCPreBinOp op de1 de2).
Proof. solve_into_dcexpr2. Qed.
Instance into_dcexpr_unop E E' e op de :
  IntoDCExpr E E' e de → IntoDCExpr E E' (c_un_op op e) (dCUnOp op de).
Proof. intros [-> ??]; by split. Qed.
Instance into_dcexpr_par E E' E'' e1 e2 de1 de2 :
  IntoDCExpr E E' e1 de1 → IntoDCExpr E' E'' e2 de2 →
  IntoDCExpr E E'' (e1 |||ᶜ e2) (dCPar de1 de2).
Proof. solve_into_dcexpr2. Qed.
Instance into_dcexpr_while E E' E'' e1 e2 de1 de2 :
  IntoDCExpr E E' e1 de1 → IntoDCExpr E' E'' e2 de2 →
  IntoDCExpr E E'' (whileᶜ(e1) { e2 }) (dCWhile de1 de2).
Proof. solve_into_dcexpr2. Qed.
Instance into_dcexpr_whileV E E' E'' e1 e2 de1 de2 :
  IntoDCExpr E E' e1 de1 → IntoDCExpr E' E'' e2 de2 →
  IntoDCExpr E E'' (whileVᶜ(e1) { e2 }) (dCWhileV de1 de2).
Proof. solve_into_dcexpr2. Qed.
Instance into_dcexpr_call E E' E'' e1 e2 de1 de2 :
  IntoDCExpr E E' e1 de1 → IntoDCExpr E' E'' e2 de2 →
  IntoDCExpr E E'' (callᶜ e1 e2) (dCCall de1 de2).
Proof. solve_into_dcexpr2. Qed.
Instance into_dcexpr_unknown E e : IntoDCExpr E E e (dCUnknown e) | 100.
Proof. done. Qed.
