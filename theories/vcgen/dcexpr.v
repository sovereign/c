From iris_c.c_translation Require Export translation.
Local Open Scope Z_scope.

Definition known_locs := list cloc.

Inductive Z_or_nat :=
  | IntZ : Z → Z_or_nat
  | IntNat : nat → Z_or_nat.
Record dint := dInt { dint_known : Z; dint_unknown : option Z_or_nat }.

Instance Z_or_nat_eq_dec : EqDecision Z_or_nat.
Proof. solve_decision. Defined.
Instance dint_eq_dec : EqDecision dint.
Proof. solve_decision. Defined.

Record dloc := dLoc { dloc_base : nat; dloc_offset : dint }.

Instance dloc_eq_dec : EqDecision dloc.
Proof. solve_decision. Defined.

Inductive dbool :=
  | dBoolKnown : bool → dbool
  | dBoolUnknown : bool → dbool.

Instance dbool_eq_dec : EqDecision dbool.
Proof. solve_decision. Defined.

Inductive dbase_lit : Type :=
  | dLitInt : dint → dbase_lit
  | dLitBool : dbool → dbase_lit
  | dLitUnit : dbase_lit
  | dLitUnknown : base_lit → dbase_lit.

Instance dbase_lit_eq_dec : EqDecision dbase_lit.
Proof. solve_decision. Defined.

Inductive dval : Type :=
  | dLitV : dbase_lit → dval
  | dPairV : dval → dval → dval
  | dLocV : dloc → dval
  | dNone : dval
  | dValUnknown : val → dval.

Instance dval_eq_dec : EqDecision dval.
Proof. solve_decision. Defined.

Inductive dexpr : Type :=
  | dEVal : dval → dexpr
  | dEVar : string → dexpr
  | dEPair : dexpr → dexpr → dexpr
  | dEFst : dexpr → dexpr
  | dESnd : dexpr → dexpr
  | dENone : dexpr
  | dEUnknown : expr → dexpr.

Inductive dcexpr : Type :=
  | dCRet : dexpr → dcexpr
  | dCSeqBind : binder → dcexpr → dcexpr → dcexpr
  | dCMutBind : binder → dcexpr → dcexpr → dcexpr
  | dCAlloc : dcexpr → dcexpr → dcexpr
  | dCLoad : dcexpr → dcexpr
  | dCStore : dcexpr → dcexpr → dcexpr
  | dCBinOp : cbin_op → dcexpr → dcexpr → dcexpr
  | dCPreBinOp : cbin_op → dcexpr → dcexpr → dcexpr
  | dCUnOp : un_op → dcexpr → dcexpr
  | dCPar : dcexpr → dcexpr → dcexpr
  | dCWhile : dcexpr → dcexpr → dcexpr
  | dCWhileV : dcexpr → dcexpr → dcexpr
  | dCCall : dcexpr → dcexpr → dcexpr
  | dCUnknown : expr → dcexpr.

Fixpoint dcexpr_size (de : dcexpr) : nat :=
  match de with
  | dCRet _ | dCUnknown _ => 1
  | dCSeqBind _ de1 de2 | dCMutBind _ de1 de2 |dCAlloc de1 de2 | dCStore de1 de2
     | dCBinOp _ de1 de2 | dCPreBinOp _ de1 de2 | dCPar de1 de2
     | dCWhile de1 de2 | dCWhileV de1 de2
     | dCCall de1 de2 => S (dcexpr_size de1 + dcexpr_size de2)
  | dCLoad de | dCUnOp _ de => S (dcexpr_size de)
  end.

Definition dloc_of_dval (dv : dval) : option dloc :=
  match dv with dLocV dl => Some dl | _ => None end.
Definition dint_of_dval (dv : dval) : option dint :=
  match dv with  dLitV (dLitInt di) => Some di | _ => None end.
Definition dknown_bool_of_dval (dv : dval) : option bool :=
  match dv with dLitV (dLitBool (dBoolKnown b)) => Some b | _ => None end.

Definition dloc_var_of_dloc (dl : dloc) : option nat :=
  match dl with
  | dLoc i (dInt 0 None) => Some i
  | _ => None
  end.
Definition dloc_var_of_dval (dv : dval) : option nat :=
  dl ← dloc_of_dval dv; dloc_var_of_dloc dl.

Definition nat_of_dint (di : dint) : option (nat * option nat) :=
  match di with
  | dInt i None => guard (0 ≤ i); Some (Z.to_nat i, None)
  | dInt i (Some (IntNat j)) => guard (0 ≤ i); Some (Z.to_nat i, Some j)
  | _ => None
  end.
Definition succ_nat_of_dint (di : dint) : option dint :=
  ''(i,mj) ← nat_of_dint di;
  match i with O => None | S i => Some (dInt i (IntNat <$> mj)) end.

Definition nat_of_dval (dv : dval) : option (nat * option nat) :=
  dl ← dint_of_dval dv; nat_of_dint dl.
Definition succ_nat_of_dval (dv : dval) : option dint :=
  dl ← dint_of_dval dv; succ_nat_of_dint dl.

(** Well-formedness *)
Definition dloc_var_wf (E : known_locs) (i : nat) : bool :=
  bool_decide (is_Some (E !! i)).

Definition dloc_wf (E : known_locs) (dl : dloc) : bool :=
  dloc_var_wf E (dloc_base dl).

Fixpoint dval_wf (E : known_locs) (dv : dval) : bool :=
  match dv with
  | dPairV dv1 dv2 => dval_wf E dv1 && dval_wf E dv2
  | dLocV dl => dloc_wf E dl
  | _  => true
  end.

Fixpoint dexpr_wf (E : known_locs) (de : dexpr) : bool :=
  match de with
  | dEVal dv => dval_wf E dv
  | dEVar _ | dEUnknown _ | dENone => true
  | dEFst de | dESnd de => dexpr_wf E de
  | dEPair de1 de2 => dexpr_wf E de1 && dexpr_wf E de2
  end.

Fixpoint dcexpr_wf (E : known_locs) (de : dcexpr) : bool :=
  match de with
  | dCRet de => dexpr_wf E de
  | dCSeqBind _ de1 de2 | dCMutBind _ de1 de2 => dcexpr_wf E de1 && dcexpr_wf E de2
  | dCLoad de1 | dCUnOp _ de1 => dcexpr_wf E de1
  | dCAlloc de1 de2 | dCStore de1 de2 | dCBinOp _ de1 de2
  | dCPreBinOp _ de1 de2 | dCWhile de1 de2 | dCWhileV de1 de2
     | dCPar de1 de2 | dCCall de1 de2 => dcexpr_wf E de1 && dcexpr_wf E de2
  | dCUnknown e => true
  end.

(** Interpretation *)
Definition Z_or_nat_interp (x : Z_or_nat) : Z :=
  match x with IntZ x => x | IntNat x => x end.
Arguments Z_or_nat_interp !_ /.

(* Defined in such a way that the interpretation is normalized. The lemma
[dint_interp_alt] proves correspondence to an alternative definition that is
better for proofs. *)
Definition dint_interp' (di : dint) : option Z :=
  match di with
  | dInt 0 None => None
  | dInt (Zpos p) None => Some (Z.of_nat (Pos.to_nat p))
  | dInt x None => Some x
  | dInt 0 (Some y) => Some (Z_or_nat_interp y)
  | dInt (Zpos p) (Some (IntNat n)) => Some (Z.of_nat (Pos.to_nat p + n))
  | dInt x (Some y) => Some (x + Z_or_nat_interp y)
  end.
Arguments dint_interp' !_ / : simpl nomatch.
Arguments Pos.to_nat !_ /.

Definition dint_interp (di : dint) : Z :=
  default 0 (dint_interp' di).
Arguments dint_interp !_ / : simpl nomatch.

Lemma dint_interp_alt di :
  dint_interp di = dint_known di + from_option Z_or_nat_interp 0 (dint_unknown di).
Proof. destruct di as [[] [[]|]]=> //=; lia. Qed.

Definition dloc_var_interp (E : known_locs) (i : nat) : cloc :=
  default inhabitant (E !! i).
Arguments dloc_var_interp !_ !_ /.

Definition dloc_interp (E : known_locs) (dl : dloc) : cloc :=
  match dl with
  | dLoc i dj =>
     match dint_interp' dj with
     | Some j => dloc_var_interp E i +∗ j
     | None => dloc_var_interp E i
     end
  end.
Arguments dloc_interp _ !_ / : simpl nomatch.
Lemma dloc_interp_alt E dl :
  dloc_interp E dl = dloc_var_interp E (dloc_base dl) +∗ dint_interp (dloc_offset dl).
Proof. destruct dl as [? [[] [[]|]]]=> //=. Qed.

Definition dbool_interp (db : dbool) : bool :=
  match db with
  | dBoolKnown b => b
  | dBoolUnknown b => b
  end.
Arguments dbool_interp !_ /.

Definition dbase_lit_interp (l : dbase_lit) : base_lit :=
  match l with
  | dLitInt x => LitInt (dint_interp x)
  | dLitBool b => LitBool (dbool_interp b)
  | dLitUnit => LitUnit
  | dLitUnknown l => l
  end.
Arguments dbase_lit_interp !_ /.

Fixpoint dval_interp (E : known_locs) (v : dval) : val :=
  match v with
  | dLitV l => LitV (dbase_lit_interp l)
  | dPairV dv1 dv2 => PairV (dval_interp E dv1) (dval_interp E dv2)
  | dLocV dl => cloc_to_val (dloc_interp E dl)
  | dNone => NONEV
  | dValUnknown v => v
  end.
Arguments dval_interp _ !_ /.

Fixpoint dexpr_interp (E : known_locs) (de : dexpr) : expr :=
  match de with
  | dEVal dv => dval_interp E dv
  | dEVar x => Var x
  | dEPair de1 de2 => (dexpr_interp E de1, dexpr_interp E de2)
  | dEFst de => Fst (dexpr_interp E de)
  | dESnd de => Snd (dexpr_interp E de)
  | dENone => NONE
  | dEUnknown e => e
  end.

Fixpoint dcexpr_interp (E : known_locs) (de : dcexpr) : expr :=
  match de with
  | dCRet de => c_ret (dexpr_interp E de)
  | dCSeqBind x de1 de2 => x ←ᶜ (dcexpr_interp E de1) ;ᶜ (dcexpr_interp E de2)
  | dCMutBind x de1 de2 => x ←mutᶜ (dcexpr_interp E de1) ;ᶜ (dcexpr_interp E de2)
  | dCAlloc de1 de2 => allocᶜ (dcexpr_interp E de1, dcexpr_interp E de2)
  | dCLoad de1 => c_load (dcexpr_interp E de1)
  | dCStore de1 de2 => c_store (dcexpr_interp E de1) (dcexpr_interp E de2)
  | dCBinOp op de1 de2 => c_bin_op op (dcexpr_interp E de1) (dcexpr_interp E de2)
  | dCPreBinOp op de1 de2 => c_pre_bin_op op (dcexpr_interp E de1) (dcexpr_interp E de2)
  | dCUnOp op de => c_un_op op (dcexpr_interp E de)
  | dCPar de1 de2 => dcexpr_interp E de1 |||ᶜ dcexpr_interp E de2
  | dCWhile de1 de2 => whileᶜ (dcexpr_interp E de1) { dcexpr_interp E de2 }
  | dCWhileV de1 de2 => whileVᶜ (dcexpr_interp E de1) { dcexpr_interp E de2 }
  | dCCall de1 de2 => callᶜ (dcexpr_interp E de1) (dcexpr_interp E de2)
  | dCUnknown e1 => e1
  end.

(* Operations *)
Definition dbool_and (db1 db2 : dbool) : dbool :=
  match db1, db2 with
  | dBoolKnown false, _ => dBoolKnown false
  | dBoolKnown true, _ => db2
  | dBoolUnknown _, dBoolKnown false => dBoolKnown false
  | dBoolUnknown _, dBoolKnown true => db1
  | dBoolUnknown b1, dBoolUnknown b2 => dBoolUnknown (b1 && b2)
  end.
Arguments dbool_and !_ _ / : simpl nomatch.

Definition dbool_or (db1 db2 : dbool) : dbool :=
  match db1, db2 with
  | dBoolKnown true, _ => dBoolKnown true
  | dBoolKnown false, _ => db2
  | dBoolUnknown _, dBoolKnown true => dBoolKnown true
  | dBoolUnknown _, dBoolKnown false => db1
  | dBoolUnknown b1, dBoolUnknown b2 => dBoolUnknown (b1 || b2)
  end.
Arguments dbool_or !_ _ / : simpl nomatch.

Definition dbool_xor (db1 db2 : dbool) : dbool :=
  match db1, db2 with
  | dBoolKnown b1, dBoolKnown b2 => dBoolKnown (xorb b1 b2)
  | dBoolKnown b1, dBoolUnknown b2 => dBoolUnknown (xorb b1 b2)
  | dBoolUnknown b1, (dBoolKnown b2 | dBoolUnknown b2) => dBoolUnknown (xorb b1 b2)
  end.
Arguments dbool_xor !_ _ / : simpl nomatch.

Definition dbool_eq (db1 db2 : dbool) : dbool :=
  match db1, db2 with
  | dBoolKnown b1, dBoolKnown b2 => dBoolKnown (bool_decide (b1 = b2))
  | dBoolKnown b1, dBoolUnknown b2 => dBoolUnknown (bool_decide (b1 = b2))
  | dBoolUnknown b1, (dBoolKnown b2 | dBoolUnknown b2) => dBoolUnknown (bool_decide (b1 = b2))
  end.
Arguments dbool_eq !_ _ / : simpl nomatch.

Definition dbool_neg (db : dbool) : dbool :=
  match db with
  | dBoolKnown b => dBoolKnown (negb b)
  | dBoolUnknown b => dBoolUnknown (negb b)
  end.
Arguments dbool_neg !_ /.

Definition dint_plus (di1 di2 : dint) : dint :=
  match di1, di2 with
  | dInt x1 None, dInt x2 None => dInt (x1 + x2) None
  | dInt x1 (Some y1), dInt x2 None => dInt (x1 + x2) (Some y1)
  | dInt x1 None, dInt x2 (Some y2) => dInt (x1 + x2) (Some y2)
  | dInt x1 (Some (IntNat y1)), dInt x2 (Some (IntNat y2)) =>
     dInt (x1 + x2) (Some (IntNat (y1 + y2)))
  | dInt x1 (Some y1), dInt x2 (Some y2) =>
     dInt (x1 + x2) (Some (IntZ (Z_or_nat_interp y1 + Z_or_nat_interp y2)))
  end.
Arguments dint_plus !_ !_ / : simpl nomatch.

Definition dint_minus (di1 di2 : dint) : dint :=
  match di1, di2 with
  | dInt x1 None, dInt x2 None => dInt (x1 - x2) None
  | dInt x1 (Some y1), dInt x2 None => dInt (x1 - x2) (Some y1)
  | dInt x1 None, dInt x2 (Some y2) =>
     dInt (x1 - x2) (Some (IntZ (-Z_or_nat_interp y2)))
  | dInt x1 (Some y1), dInt x2 (Some y2) =>
     dInt (x1 - x2) (Some (IntZ (Z_or_nat_interp y1 - Z_or_nat_interp y2)))
  end.
Arguments dint_minus !_ !_ / : simpl nomatch.

Definition dint_opp (di : dint) : dint :=
  match di with
  | dInt x None => dInt (-x) None
  | dInt x (Some y) => dInt (-x) (Some (IntZ (-Z_or_nat_interp y)))
  end.
Arguments dint_opp !_ / : simpl nomatch.

Definition dint_lt (di1 di2 : dint) : dbool :=
  match di1, di2 with
  | dInt x1 None, dInt x2 None => dBoolKnown (bool_decide (x1 < x2))
  | dInt x1 None, dInt x2 (Some (IntNat _)) =>
     if bool_decide (x1 < x2) then dBoolKnown true
     else dBoolUnknown (bool_decide (dint_interp di1 < dint_interp di2))
  | _, _ => dBoolUnknown (bool_decide (dint_interp di1 < dint_interp di2))
  end.
Arguments dint_lt !_ !_ / : simpl nomatch.

Definition dint_le (di1 di2 : dint) : dbool :=
  match di1, di2 with
  | dInt x1 None, dInt x2 None => dBoolKnown (bool_decide (x1 ≤ x2))
  | _, _ => dBoolUnknown (bool_decide (dint_interp di1 ≤ dint_interp di2))
  end.
Arguments dint_le !_ !_ / : simpl nomatch.

Definition dint_eq (di1 di2 : dint) : dbool :=
  match di1, di2 with
  | dInt x1 None, dInt x2 None => dBoolKnown (bool_decide (x1 = x2))
  | dInt x1 (Some (IntNat _)), dInt x2 None =>
     if decide (x2 < x1)
     then dBoolKnown false
     else dBoolUnknown (bool_decide (dint_interp di1 = dint_interp di2))
  | dInt x1 None, dInt x2 (Some (IntNat _)) =>
     if decide (x1 < x2)
     then dBoolKnown false
     else dBoolUnknown (bool_decide (dint_interp di1 = dint_interp di2))
  | _, _ => dBoolUnknown (bool_decide (dint_interp di1 = dint_interp di2))
  end.
Arguments dint_eq !_ !_ / : simpl nomatch.

Definition dloc_plus (dl : dloc) (dj : dint) : dloc :=
  match dl with
  | dLoc dl di => dLoc dl (dint_plus di dj)
  end.
Arguments dloc_plus !_ !_ / : simpl nomatch.

Definition dloc_eq (E : known_locs) (dl1 dl2 : dloc) : dbool :=
  if decide (dloc_base dl1 = dloc_base dl2)
  then dint_eq (dloc_offset dl1) (dloc_offset dl2)
  else dBoolUnknown (bool_decide (dloc_interp E dl1 = dloc_interp E dl2)).
Arguments dloc_eq _ !_ !_ / : simpl nomatch.

Definition dloc_lt (E : known_locs) (dl1 dl2 : dloc) : dbool :=
  if decide (dloc_base dl1 = dloc_base dl2)
  then dint_lt (dloc_offset dl1) (dloc_offset dl2)
  else dBoolUnknown (cloc_lt (dloc_interp E dl1) (dloc_interp E dl2)).
Arguments dloc_lt _ !_ !_ / : simpl nomatch.

Definition dbin_op_eval_bool (op : bin_op) (db1 db2 : dbool) : option dbase_lit :=
  match op with
  | AndOp => Some $ dLitBool $ dbool_and db1 db2
  | OrOp => Some $ dLitBool $ dbool_or db1 db2
  | XorOp => Some $ dLitBool $ dbool_xor db1 db2
  | EqOp => Some $ dLitBool $ dbool_eq db1 db2
  | _ => None
  end.
Arguments dbin_op_eval_bool !_ _ _ /.

Definition dbin_op_eval_int (op : bin_op) (di1 di2 : dint) : dbase_lit :=
  match op with
  | PlusOp => dLitInt $ dint_plus di1 di2
  | MinusOp => dLitInt $ dint_minus di1 di2
  | EqOp => dLitBool $ dint_eq di1 di2
  | LtOp => dLitBool $ dint_lt di1 di2
  | LeOp => dLitBool $ dint_le di1 di2
  | _ => dLitUnknown $ bin_op_eval_int op (dint_interp di1) (dint_interp di2)
         (* Do better in case both operands are known *)
  end.
Arguments dbin_op_eval_int !_ _ _ /.

Definition dbase_lit_eq (dl1 dl2 : dbase_lit) : dbool :=
  match dl1, dl2 with
  | dLitInt di1, dLitInt di2 => dint_eq di1 di2
  | dLitBool db1, dLitBool db2 => dbool_eq db1 db2
  | dLitUnit, dLitUnit => dBoolKnown true
  | dLitInt _, (dLitUnit | dLitBool _)
     | dLitBool _, (dLitInt _ | dLitUnit)
     | dLitUnit, (dLitInt _ | dLitBool _) => dBoolKnown false (* different known constructors *)
  | _, _ => dBoolUnknown (bool_decide (dbase_lit_interp dl1 = dbase_lit_interp dl2))
  end.
Arguments dbase_lit_eq !_ !_ / : simpl nomatch.

Fixpoint dval_eq (E : known_locs) (dv1 dv2 : dval) : dbool :=
  match dv1, dv2 with
  | dLitV dl1, dLitV dl2 => dbase_lit_eq dl1 dl2
  | dNone, dNone => dBoolKnown true
  | dPairV dv1 dv1', dPairV dv2 dv2' => dbool_and (dval_eq E dv1 dv2) (dval_eq E dv1' dv2')
  | dLocV dl1, dLocV dl2 => dloc_eq E dl1 dl2
  | dLitV _, (dNone | dPairV _ _ | dLocV _)
     | dNone, (dLitV _ | dPairV _ _ | dLocV _)
     | dPairV _ _, (dNone | dLitV _ | dLocV _)
     | dLocV _, (dNone | dLitV _ | dPairV _ _) => dBoolKnown false (* different known constructors *)
  | _, _ => dBoolUnknown (bool_decide (dval_interp E dv1 = dval_interp E dv2))
  end.
Arguments dval_eq _ !_ !_ / : simpl nomatch.

Definition dbin_op_eval (E : known_locs) (op : bin_op) (dv1 dv2 : dval) : option dval :=
  if decide (op = EqOp) then Some $ dLitV $ dLitBool $ dval_eq E dv1 dv2 else
  match dv1, dv2 with
  | dLitV (dLitInt di1), dLitV (dLitInt di2) => Some $ dLitV $ dbin_op_eval_int op di1 di2
  | dLitV (dLitBool db1), dLitV (dLitBool db2) => dLitV <$> dbin_op_eval_bool op db1 db2
  | _, _ => None
  end.
Arguments dbin_op_eval _ !_ _ _ / : simpl nomatch.

Definition dcbin_op_eval (E : known_locs) (op : cbin_op) (dv1 dv2 : dval) : option dval :=
  match op with
  | CBinOp op' => dbin_op_eval E op' dv1 dv2
  | PtrPlusOp =>
     dl ← dloc_of_dval dv1;
     di ← dint_of_dval dv2;
     Some $ dLocV $ dloc_plus dl di
  | PtrLtOp =>
     dl1 ← dloc_of_dval dv1;
     dl2 ← dloc_of_dval dv2;
     Some $ dLitV $ dLitBool $ dloc_lt E dl1 dl2
  end.
Arguments dcbin_op_eval _ !_ !_ !_ / : simpl nomatch.

Definition dun_op_eval (E : known_locs) (op : un_op) (dv : dval) : option dval :=
  match op, dv with
  | NegOp, dLitV (dLitBool db) => Some $ dLitV $ dLitBool $ dbool_neg db
  | MinusUnOp, dLitV (dLitInt di) => Some $ dLitV $ dLitInt $ dint_opp di
  | _, _ => None
  end.
Arguments dun_op_eval _ !_ !_ / : simpl nomatch.

(** Correctness proofs *)
Lemma dloc_of_dval_Some dv dl : dloc_of_dval dv = Some dl → dv = dLocV dl.
Proof. intros. by destruct dv; simplify_eq/=. Qed.
Lemma dint_of_dval_Some dv di : dint_of_dval dv = Some di → dv = dLitV (dLitInt di).
Proof. intros. by destruct dv as [[]| | | |]; simplify_eq/=. Qed.

Lemma dknown_bool_of_dval_Some dv b :
  dknown_bool_of_dval dv = Some b → dv = dLitV (dLitBool (dBoolKnown b)).
Proof.
  rewrite /dknown_bool_of_dval. intros; by repeat (simplify_eq/= || case_match).
Qed.
Lemma dknown_bool_of_dval_correct E dv b :
  dknown_bool_of_dval dv = Some b → dval_interp E dv = #b.
Proof. by intros ->%dknown_bool_of_dval_Some. Qed.

Lemma dloc_var_of_dloc_Some dl i :
  dloc_var_of_dloc dl = Some i → dl = dLoc i (dInt 0 None).
Proof.
  rewrite /dloc_var_of_dloc. intros; by repeat (simplify_eq/= || case_match).
Qed.
Lemma dloc_var_of_dval_Some dv i :
  dloc_var_of_dval dv = Some i → dv = dLocV (dLoc i (dInt 0 None)).
Proof. by intros (dl&->%dloc_of_dval_Some&->%dloc_var_of_dloc_Some)%bind_Some. Qed.
Lemma dloc_var_of_dval_wf E dv i :
  dloc_var_of_dval dv = Some i → dval_wf E dv → dloc_var_wf E i.
Proof. by intros ->%dloc_var_of_dval_Some. Qed.
Lemma dloc_var_of_dval_correct E dv i :
  dloc_var_of_dval dv = Some i → dval_interp E dv = cloc_to_val (dloc_var_interp E i).
Proof. by intros ->%dloc_var_of_dval_Some. Qed.

Lemma nat_of_dint_Some di i mj :
  nat_of_dint di = Some (i,mj) → di = dInt i (IntNat <$> mj).
Proof.
  destruct di as [i' [[j|]|]]; intros;
    repeat (simplify_eq/= || case_option_guard); by rewrite Z2Nat.id.
Qed.
Lemma nat_of_dval_Some dv i mj :
  nat_of_dval dv = Some (i,mj) → dv = dLitV (dLitInt (dInt i (IntNat <$> mj))).
Proof. by intros (di&->%dint_of_dval_Some&->%nat_of_dint_Some)%bind_Some. Qed.

Lemma succ_nat_of_dint_Some di dj :
  succ_nat_of_dint di = Some dj →
  dint_interp di = S (Z.to_nat (dint_interp dj)).
Proof.
  intros ([[|i] [j|]] & ->%nat_of_dint_Some & [= <-])%bind_Some;
    rewrite !dint_interp_alt /=.
  - rewrite -Z2Nat.inj_succ; last by lia. rewrite Z2Nat.id; lia.
  - rewrite -Z2Nat.inj_succ; last by lia. rewrite Z2Nat.id; lia.
Qed.
Lemma succ_nat_of_dval_Some E dv di :
  succ_nat_of_dval dv = Some di →
  dval_interp E dv = #(S (Z.to_nat (dint_interp di))).
Proof. by intros (dl&->%dint_of_dval_Some&<-%succ_nat_of_dint_Some)%bind_Some. Qed.

Lemma dbool_and_correct db1 db2 :
  dbool_interp (dbool_and db1 db2) = dbool_interp db1 && dbool_interp db2.
Proof. by destruct db1 as [[]|[]], db2 as [[]|[]]. Qed.
Lemma dbool_or_correct db1 db2 :
  dbool_interp (dbool_or db1 db2) = dbool_interp db1 || dbool_interp db2.
Proof. by destruct db1 as [[]|[]], db2 as [[]|[]]. Qed.
Lemma dbool_xor_correct db1 db2 :
  dbool_interp (dbool_xor db1 db2) = xorb (dbool_interp db1) (dbool_interp db2).
Proof. by destruct db1 as [[]|[]], db2 as [[]|[]]. Qed.
Lemma dbool_eq_correct db1 db2 :
  dbool_interp (dbool_eq db1 db2) = bool_decide (dbool_interp db1 = dbool_interp db2).
Proof. by destruct db1 as [[]|[]], db2 as [[]|[]]. Qed.
Lemma dbool_neg_correct db :
  dbool_interp (dbool_neg db) = negb (dbool_interp db).
Proof. by destruct db as [[]|[]]. Qed.

Lemma dint_plus_correct di1 di2 :
  dint_interp (dint_plus di1 di2) = dint_interp di1 + dint_interp di2.
Proof.
  rewrite !dint_interp_alt. destruct di1 as [? [[]|]], di2 as [? [[]|]]; simpl; lia.
Qed.
Lemma dint_minus_correct di1 di2 :
  dint_interp (dint_minus di1 di2) = dint_interp di1 - dint_interp di2.
Proof.
  rewrite !dint_interp_alt. destruct di1 as [? [[]|]], di2 as [? [[]|]]; simpl; lia.
Qed.
Lemma dint_opp_correct di :
  dint_interp (dint_opp di) = -dint_interp di.
Proof. rewrite !dint_interp_alt. destruct di as [? [[]|]]; simpl; lia. Qed.
Lemma dint_lt_correct di1 di2 :
  dbool_interp (dint_lt di1 di2) = bool_decide (dint_interp di1 < dint_interp di2).
Proof.
  rewrite /dint_lt !dint_interp_alt.
  destruct di1 as [? [[]|]], di2 as [? [[]|]]; simpl; repeat case_bool_decide; auto with lia.
Qed.
Lemma dint_le_correct di1 di2 :
  dbool_interp (dint_le di1 di2) = bool_decide (dint_interp di1 ≤ dint_interp di2).
Proof.
  rewrite /dint_le !dint_interp_alt.
  destruct di1 as [? [[]|]], di2 as [? [[]|]]; simpl; repeat case_bool_decide; auto with lia.
Qed.
Lemma dint_eq_correct di1 di2 :
  dbool_interp (dint_eq di1 di2) = bool_decide (dint_interp di1 = dint_interp di2).
Proof.
  rewrite /dint_eq !dint_interp_alt.
  destruct di1 as [? [[]|]], di2 as [? [[]|]]; simpl;
    repeat (case_decide || case_bool_decide); auto with lia.
Qed.

Lemma dloc_plus_correct E dl di :
  dloc_interp E (dloc_plus dl di) = dloc_interp E dl +∗ dint_interp di.
Proof.
  rewrite !dloc_interp_alt. by destruct dl as [dl dj]; rewrite /= dint_plus_correct cloc_plus_plus.
Qed.
Lemma dloc_lt_correct E dl1 dl2 :
  dbool_interp (dloc_lt E dl1 dl2) = cloc_lt (dloc_interp E dl1) (dloc_interp E dl2).
Proof.
  unfold dloc_lt. destruct (decide _) as [H|]; last done.
  rewrite dint_lt_correct /cloc_lt !dloc_interp_alt /=.
  rewrite H. repeat case_bool_decide; naive_solver eauto with lia.
Qed.
Lemma dloc_eq_correct E dl1 dl2 :
  dbool_interp (dloc_eq E dl1 dl2) = bool_decide (dloc_interp E dl1 = dloc_interp E dl2).
Proof.
  unfold dloc_eq. destruct (decide _) as [H|]; last done.
  rewrite dint_eq_correct !dloc_interp_alt /=.
  rewrite H. apply bool_decide_iff; split; [by intros ->|by intros ?%(inj _)].
Qed.

Lemma dbin_op_eval_int_correct op di1 di2 :
  dbase_lit_interp (dbin_op_eval_int op di1 di2) =
  bin_op_eval_int op (dint_interp di1) (dint_interp di2).
Proof.
  destruct op; f_equal/=; auto using dint_plus_correct, dint_minus_correct,
    dint_eq_correct, dint_lt_correct, dint_le_correct.
Qed.

Lemma dbin_op_eval_bool_correct op db1 db2 dl :
  dbin_op_eval_bool op db1 db2 = Some dl →
  bin_op_eval_bool op (dbool_interp db1) (dbool_interp db2) = Some (dbase_lit_interp dl).
Proof.
  destruct op; intros; simplify_eq/=; do 2 f_equal/=;
    auto using dbool_and_correct, dbool_or_correct,
    dbool_xor_correct, dbool_eq_correct.
Qed.

Lemma dbase_lit_eq_correct dl1 dl2 :
  dbool_interp (dbase_lit_eq dl1 dl2) =
  bool_decide (dbase_lit_interp dl1 = dbase_lit_interp dl2).
Proof.
  destruct dl1, dl2=> //=.
  - rewrite dint_eq_correct. apply bool_decide_iff. naive_solver congruence.
  - rewrite dbool_eq_correct. apply bool_decide_iff. naive_solver congruence.
Qed.

Lemma dval_eq_correct E dv1 dv2 :
  dbool_interp (dval_eq E dv1 dv2) =
  bool_decide (dval_interp E dv1 = dval_interp E dv2).
Proof.
  revert dv1. induction dv2; intros []=> //=; try by rewrite cloc_to_val_eq.
  - rewrite dbase_lit_eq_correct. apply bool_decide_iff. naive_solver congruence.
  - rewrite dbool_and_correct. rewrite IHdv2_1 IHdv2_2.
    repeat case_bool_decide; intuition congruence.
  - rewrite dloc_eq_correct.
    apply bool_decide_iff; split; [by intros ->|by intros ?%(inj _)].
Qed.

Lemma dbin_op_eval_correct E op dv1 dv2 dw :
  dbin_op_eval E op dv1 dv2 = Some dw →
  bin_op_eval op (dval_interp E dv1) (dval_interp E dv2) = Some (dval_interp E dw).
Proof.
  rewrite /bin_op_eval /dbin_op_eval=> ?. destruct (decide (op = _)); simplify_eq/=.
  { by rewrite dval_eq_correct. }
  destruct dv1 as [[]| | | |], dv2 as [[]| | | |]; simplify_option_eq.
  - by rewrite dbin_op_eval_int_correct.
  - by erewrite dbin_op_eval_bool_correct by done.
Qed.

Lemma dcbin_op_eval_correct op E dv1 dv2 dw :
  dcbin_op_eval E op dv1 dv2 = Some dw →
  cbin_op_eval op (dval_interp E dv1) (dval_interp E dv2) = Some (dval_interp E dw).
Proof.
  rewrite /dcbin_op_eval; destruct op; intros; simplify_option_eq;
    repeat match goal with
    | H : dloc_of_dval _ = _ |- _ => apply dloc_of_dval_Some in H as ->
    | H : dint_of_dval _ = _ |- _ => apply dint_of_dval_Some in H as ->
    end; simpl.
  - by apply dbin_op_eval_correct.
  - by rewrite cloc_of_to_val /= dloc_plus_correct.
  - by rewrite !cloc_of_to_val /= dloc_lt_correct.
Qed.

Lemma dun_op_eval_correct E op dv w :
  dun_op_eval E op dv = Some w →
  un_op_eval op (dval_interp E dv) = Some (dval_interp E w).
Proof.
  intros; destruct op, dv as [[]| | | |]; simplify_eq/=.
  - by rewrite dbool_neg_correct.
  - by rewrite dint_opp_correct.
Qed.

(** Well-formedness w.r.t. known_locs *)
Lemma dloc_var_wf_mono E E' i :
  dloc_var_wf E i → E `prefix_of` E' → dloc_var_wf E' i.
Proof.
  rewrite /dloc_var_wf !bool_decide_spec.
  intros [v ?] [E'' ->]. exists v; by apply lookup_app_l_Some.
Qed.

Lemma dloc_wf_mono E E' dl :
  dloc_wf E dl → E `prefix_of` E' → dloc_wf E' dl.
Proof. apply dloc_var_wf_mono. Qed.

Lemma dval_wf_mono E E' dv :
  dval_wf E dv → E `prefix_of` E' → dval_wf E' dv.
Proof. induction dv; naive_solver eauto using dloc_wf_mono. Qed.

Lemma dexpr_wf_mono E E' de :
  dexpr_wf E de → E `prefix_of` E' → dexpr_wf E' de.
Proof. induction de; naive_solver eauto using dval_wf_mono. Qed.

Lemma dcexpr_wf_mono E E' de :
  dcexpr_wf E de → E `prefix_of` E' → dcexpr_wf E' de.
Proof. induction de; naive_solver eauto using dexpr_wf_mono. Qed.

Lemma dloc_of_dval_Some_wf E dv dl :
  dval_wf E dv → dloc_of_dval dv = Some dl → dloc_wf E dl.
Proof. destruct dv; naive_solver. Qed.

Lemma dloc_plus_wf E dl di : dloc_wf E dl → dloc_wf E (dloc_plus dl di).
Proof. by destruct dl. Qed.

Lemma dbin_op_eval_Some_wf E dv1 dv2 op dw:
  dbin_op_eval E op dv1 dv2 = Some dw →
  dval_wf E dv1 → dval_wf E dv2 → dval_wf E dw.
Proof.
  rewrite /dbin_op_eval; intros; simplify_option_eq; auto.
  destruct dv1 as [[]| | | |], dv2 as [[]| | | |]; simplify_option_eq; auto.
Qed.

Lemma dcbin_op_eval_Some_wf E dv1 dv2 op dw:
  dcbin_op_eval E op dv1 dv2 = Some dw →
  dval_wf E dv1 → dval_wf E dv2 → dval_wf E dw.
Proof.
  rewrite /dcbin_op_eval. intros; destruct op; simplify_option_eq;
    eauto using dloc_plus_wf, dloc_of_dval_Some_wf, dbin_op_eval_Some_wf.
Qed.

Lemma dun_op_eval_Some_wf E dv op dw:
  dun_op_eval E op dv = Some dw → dval_wf E dv → dval_wf E dw.
Proof. destruct op, dv as [[]| | | |]; naive_solver. Qed.

Lemma dloc_var_interp_mono E E' dl :
  dloc_var_wf E dl → E `prefix_of` E' →
  dloc_var_interp E dl = dloc_var_interp E' dl.
Proof.
  rewrite /dloc_var_wf /dloc_var_interp !bool_decide_spec.
  intros [v ?] [E'' ->]. rewrite lookup_app_l; eauto using lookup_lt_is_Some_1.
Qed.

Lemma dloc_interp_mono E E' dl :
  dloc_wf E dl → E `prefix_of` E' →
  dloc_interp E dl = dloc_interp E' dl.
Proof.
  rewrite !dloc_interp_alt=> ??. destruct dl as [dl i].
  f_equal. by apply dloc_var_interp_mono.
Qed.

Lemma dval_interp_mono E E' dv :
  dval_wf E dv → E `prefix_of` E' → dval_interp E dv = dval_interp E' dv.
Proof. induction dv; naive_solver eauto using dloc_interp_mono with f_equal. Qed.

Lemma dexpr_interp_mono E E' de :
   dexpr_wf E de → E `prefix_of` E' → dexpr_interp E de = dexpr_interp E' de.
Proof. induction de; naive_solver eauto using dval_interp_mono with f_equal. Qed.

Lemma dcexpr_interp_mono E E' de :
  dcexpr_wf E de → E `prefix_of` E' →
  dcexpr_interp E de = dcexpr_interp E' de.
Proof. induction de; try naive_solver eauto 10 using dexpr_interp_mono with f_equal. Qed.

(** ** Substitution *)
Fixpoint de_subst (E: known_locs) (x: string) (dv : dval) (de: dexpr) : dexpr :=
  match de with
  | dEVal dv => dEVal dv
  | dEVar y  => if decide (x = y) then dEVal dv else de
  | dEPair de1 de2 => dEPair (de_subst E x dv de1) (de_subst E x dv de2)
  | dEFst de1 => dEFst (de_subst E x dv de1)
  | dESnd de1 => dESnd (de_subst E x dv de1)
  | dENone => dENone
  | dEUnknown e => dEUnknown (subst x (dval_interp E dv) e)
  end.

Fixpoint dce_subst (E: known_locs) (x: string) (dv : dval) (dce : dcexpr) : dcexpr :=
  match dce with
  | dCRet de1 => dCRet (de_subst E x dv de1)
  | dCSeqBind y de1 de2 =>
    if decide (BNamed x = y)
    then dCSeqBind y (dce_subst E x dv de1) de2
    else dCSeqBind y (dce_subst E x dv de1) (dce_subst E x dv de2)
  | dCMutBind y de1 de2 =>
    if decide (BNamed x = y)
    then dCMutBind y (dce_subst E x dv de1) de2
    else dCMutBind y (dce_subst E x dv de1) (dce_subst E x dv de2)
  | dCAlloc de1 de2 => dCAlloc (dce_subst E x dv de1) (dce_subst E x dv de2)
  | dCLoad de1 =>  dCLoad (dce_subst E x dv de1)
  | dCStore de1 de2 => dCStore (dce_subst E x dv de1) (dce_subst E x dv de2)
  | dCBinOp op de1 de2 =>
     dCBinOp op (dce_subst E x dv de1) (dce_subst E x dv de2)
  | dCPreBinOp op de1 de2 =>
     dCPreBinOp op (dce_subst E x dv de1) (dce_subst E x dv de2)
  | dCUnOp op de1 =>  dCUnOp op (dce_subst E x dv de1)
  | dCPar de1 de2 => dCPar (dce_subst E x dv de1) (dce_subst E x dv de2)
  | dCWhile de1 de2 => dCWhile (dce_subst E x dv de1) (dce_subst E x dv de2)
  | dCWhileV de1 de2 => dCWhileV de1 de2
  | dCCall de1 de2 => dCCall (dce_subst E x dv de1) (dce_subst E x dv de2)
  | dCUnknown e => dCUnknown (subst x (dval_interp E dv) e)
  end.

Definition dce_subst' (E: known_locs) (mx: binder) (dv : dval) (dce : dcexpr) : dcexpr :=
  match mx with BAnon => dce | BNamed x => dce_subst E x dv dce end.

Lemma dexpr_interp_subst E x de dv :
  dexpr_interp E (de_subst E x dv de) = subst x (dval_interp E dv) (dexpr_interp E de).
Proof. induction de; simplify_eq /=; repeat case_match; auto with f_equal. Qed.

Lemma dcexpr_interp_subst E x de dv :
  dcexpr_interp E (dce_subst E x dv de) = subst x (dval_interp E dv) (dcexpr_interp E de).
Proof.
  induction de; repeat (simplify_eq /= || case_decide);
    naive_solver eauto 10 using dexpr_interp_subst with f_equal.
Qed.

Lemma dcexpr_interp_subst' E mx de dv :
  dcexpr_interp E (dce_subst' E mx dv de) = subst' mx (dval_interp E dv) (dcexpr_interp E de).
Proof. destruct mx; simpl; auto using dcexpr_interp_subst. Qed.

Lemma de_subst_wf E x dv1 de2 :
  dval_wf E dv1 → dexpr_wf E de2 → dexpr_wf E (de_subst E x dv1 de2).
Proof.
  induction de2; intros; repeat (simplify_eq /= || case_decide); naive_solver.
Qed.

Lemma dce_subst_wf E x dv1 de2 :
  dval_wf E dv1 → dcexpr_wf E de2 → dcexpr_wf E (dce_subst E x dv1 de2).
Proof.
  induction de2; intros; repeat (simplify_eq /= || case_decide);
    naive_solver auto using de_subst_wf.
Qed.

Lemma dce_subst_wf' E mx dv1 de2 :
  dval_wf E dv1 → dcexpr_wf E de2 → dcexpr_wf E (dce_subst' E mx dv1 de2).
Proof. destruct mx; simpl; auto using dce_subst_wf. Qed.

Lemma de_subst_mono E E' x dv1 de2 :
  dval_wf E dv1 → dexpr_wf E de2 → E `prefix_of` E' →
  de_subst E x dv1 de2 = de_subst E' x dv1 de2.
Proof.
  induction de2; intros; repeat (simplify_eq /= || case_decide);
    naive_solver eauto using dval_interp_mono with f_equal.
Qed.

Lemma dce_subst_mono E E' x dv1 de2 :
  dval_wf E dv1 → dcexpr_wf E de2 → E `prefix_of` E' →
  dce_subst E x dv1 de2 = dce_subst E' x dv1 de2.
Proof.
  induction de2; intros; repeat (simplify_eq /= || case_decide);
    naive_solver eauto using de_subst_mono, dval_interp_mono with f_equal.
Qed.

Lemma dce_subst_mono' E E' mx dv1 de2 :
  dval_wf E dv1 → dcexpr_wf E de2 → E `prefix_of` E' →
  dce_subst' E mx dv1 de2 = dce_subst' E' mx dv1 de2.
Proof. destruct mx; simpl; auto using dce_subst_mono. Qed.
