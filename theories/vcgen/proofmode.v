From iris_c.c_translation Require Export translation.
From iris_c.vcgen Require Import vcg denv reification.
From iris.proofmode Require Import environments coq_tactics.
Import env_notations.

Class IntoDEnv `{cmonadG Σ} (E E' : known_locs) (Γin Γout : env (iProp Σ)) (m : denv) := {
  into_denv : [∗] Γin ⊢ [∗] Γout ∗ denv_interp E' m;
  into_denv_env_wf : env_wf Γin → env_wf Γout;
  into_denv_env_lookup i : Γin !! i = None → Γout !! i = None;
  into_denv_wf : denv_wf E' m;
  into_denv_mono : E `prefix_of` E';
}.

Class PropIntoDEnv `{cmonadG Σ} (E E' : known_locs) (Pin Pout : iProp Σ) (m1 m2 : denv) := {
  prop_into_denv : denv_wf E m1 → Pin ∗ denv_interp E m1 ⊢ Pout ∗ denv_interp E' m2;
  prop_into_denv_wf : denv_wf E m1 → denv_wf E' m2;
  prop_into_denv_mono : E `prefix_of` E';
}.
Arguments PropIntoDEnv {_ _} _ _ _%I _%I _.

Section tactics.
  Context `{cmonadG Σ}.
  Implicit Types P Q : iProp Σ.
  Hint Extern 0 (0 < _)%Q => apply Qp_prf.
  Hint Extern 2 (_ `prefix_of` _) => etrans; [eassumption|].

  Global Instance prop_into_denv_default E P m : PropIntoDEnv E E P P m m | 100.
  Proof. by split. Qed.
  Global Instance prop_into_denv_mapsto E E' E'' cl i lv q v dv m :
    LocLookup E E' cl i → IntoDVal E' E'' v dv →
    PropIntoDEnv E E'' (cl ↦C[lv]{q} v) emp m (denv_insert i lv q dv m).
  Proof.
    intros [??] [-> ? HE']. split; last by etrans.
    - iIntros (?) "[??]". iSplit=> //.
      iApply denv_insert_interp; eauto using denv_wf_mono.
      rewrite Q_to_of_Qp -(denv_interp_mono E); eauto.
      rewrite /dloc_var_interp (prefix_lookup E' _ _ cl) //=. iFrame.
    - intros. eapply denv_wf_insert; eauto using denv_wf_mono.
      rewrite /dloc_var_wf (prefix_lookup E' _ _ cl) //=.
  Qed.
  (* Not an instance, due to the IntoSep, this rule will loop if Pin can be
  split infinitely many times *)
  Lemma prop_into_denv_sep_aux E E' E'' Pin Pin1 Pin2 Pout1 Pout2 Pout m1 m2 m3 :
    IntoSep Pin Pin1 Pin2 →
    PropIntoDEnv E E' Pin1 Pout1 m1 m2 →
    PropIntoDEnv E' E'' Pin2 Pout2 m2 m3 →
    MakeSep Pout1 Pout2 Pout →
    PropIntoDEnv E E'' Pin Pout m1 m3.
  Proof.
    rewrite /IntoSep /MakeSep. intros HP [HP1 ??] [HP2 ??] HPout.
    split; [|eauto|by etrans].
    intros. rewrite HP (comm _ Pin1) -assoc HP1 // assoc.
    rewrite (comm _ Pin2) -assoc HP2; last by eauto. by rewrite assoc -HPout.
  Qed.
  Global Instance prop_into_denv_sep E E' E'' Pin1 Pin2 Pout1 Pout2 Pout m1 m2 m3 :
    PropIntoDEnv E E' Pin1 Pout1 m1 m2 →
    PropIntoDEnv E' E'' Pin2 Pout2 m2 m3 →
    MakeSep Pout1 Pout2 Pout →
    PropIntoDEnv E E'' (Pin1 ∗ Pin2) Pout m1 m3.
  Proof. apply prop_into_denv_sep_aux, _. Qed.
  Global Instance prop_into_denv_mapsto_list E E' E'' q cl vs v vs' Pout1 Pout2 Pout m1 m2 m3 :
    IsCons vs v vs' →
    PropIntoDEnv E E' (cl ↦C{q} v) Pout1 m1 m2 →
    PropIntoDEnv E' E'' ((cl +∗ 1) ↦C{q}∗ vs') Pout2 m2 m3 →
    MakeSep Pout1 Pout2 Pout →
    PropIntoDEnv E E'' (cl ↦C{q}∗ vs) Pout m1 m3.
  Proof. intros ?. apply prop_into_denv_sep_aux, _. Qed.

  Global Instance into_denv_nil E : IntoDEnv E E Enil Enil nil.
  Proof. split; unfold denv_interp; eauto. Qed.
  Global Instance into_denv_snoc E E' E'' Γin Γout Γout' i Pin Pout m1 m2 :
    IntoDEnv E E' Γin Γout m1 →
    PropIntoDEnv E' E'' Pin Pout m1 m2 →
    TCIf (TCEq Pout emp)%I (TCEq Γout Γout') (TCEq Γout' (Esnoc Γout i Pout)) →
    IntoDEnv E E'' (Esnoc Γin i Pin) Γout' m2.
  Proof.
    intros [HΓ ????] [HP ??] [-> ->| ->].
    - split; simpl; eauto.
      + by rewrite HΓ assoc (comm _ Pin) -assoc HP // left_id.
      + inversion_clear 1; eauto.
      + intros j. destruct (ident_beq_reflect j i) as [->|]; by eauto.
    - split; simpl; eauto.
      + by rewrite HΓ assoc (comm _ Pin) -assoc HP // assoc (comm _ Pout).
      + inversion_clear 1; constructor; eauto.
      + intros j. destruct (ident_beq_reflect j i) as [->|]; by eauto.
  Qed.

  Lemma tac_vcg E1 E2 Γp Γs_in Γs_out m c e de R Φ :
    IntoDEnv [] E1 Γs_in Γs_out m →
    IntoDCExpr E1 E2 e de →
    envs_entails (Envs Γp Γs_out c) (vcg_while E2 (dcexpr_size de) m de R
      (λ E3 m dv, wand_denv_interp E3 m (Φ (dval_interp E3 dv)))) →
    envs_entails (Envs Γp Γs_in c) (CWP e @ R {{ Φ }}).
  Proof.
    intros [HΓ ????] [-> ??]; rewrite !envs_entails_eq /= /of_envs.
    iIntros (Hentails) "(Hwf & #HΓp & HΓs)".
    iDestruct "Hwf" as %Hwf; inversion_clear Hwf; simpl in *.
    iDestruct (HΓ with "HΓs") as "[HΓs Hm]".
    iDestruct (Hentails with "[$HΓs $HΓp]") as "HΓs".
    { iPureIntro; constructor; naive_solver. }
    iApply (cwp_wand with "[-]").
    { iApply (vcg_while_correct with "[Hm] HΓs"); eauto using denv_wf_mono.
      iApply (denv_interp_mono with "Hm"); eauto. }
    rewrite /vcg_continuation. iIntros (v) "H".
    iDestruct "H" as (E3 dv m' -> ???) "[Hm HΦ]".
    by iApply (wand_denv_interp_spec with "HΦ").
  Qed.

  Lemma tac_vcg_continue E1 E2 E3 Γp Γs_in Γs_out m c v dv Φ :
    IntoDEnv E1 E2 Γs_in Γs_out m →
    IntoDVal E2 E3 v dv →
    envs_entails (Envs Γp Γs_out c) (Φ E3 m dv) →
    envs_entails (Envs Γp Γs_in c) (vcg_continuation E1 Φ v).
  Proof.
    intros [HΓ ????] [-> ??]; rewrite !envs_entails_eq /= /of_envs.
    iIntros (Hentails) "(Hwf & #HΓp & HΓs)".
    iDestruct "Hwf" as %Hwf; inversion_clear Hwf; simpl in *.
    iDestruct (HΓ with "HΓs") as "[HΓs Hm]".
    iDestruct (Hentails with "[$HΓs $HΓp]") as "HΓs".
    { iPureIntro; constructor; naive_solver. }
    iExists E3, dv, m. repeat (iSplit; first by eauto using denv_wf_mono).
    iFrame "HΓs". iApply (denv_interp_mono with "Hm"); eauto.
  Qed.
End tactics.

(* Make sure users do not see auxiliary junk *)
Arguments vcg_continuation {_ _ _ _}.

Ltac vcg :=
  iStartProof;
  eapply tac_vcg;
    [iSolveTC (* Reify the context *)
    |iSolveTC (* Reify the expression *)
    |simpl].

Ltac vcg_continue :=
  iStartProof;
  eapply tac_vcg_continue;
    [iSolveTC (* Reify the context *)
    |iSolveTC (* Reify the expression *)
    |simpl].
