From iris.algebra Require Export frac.
From Coq Require Export QArith Qcanon.

Lemma Q_plus_nonneg p q : (0 < p)%Q → (0 < q)%Q → (0 < p + q)%Q.
Proof.
  intros. apply Qlt_trans with p=> //. by rewrite -{1}(Qplus_0_r p) Qplus_lt_r.
Qed.
Lemma Q_div_nonneg p n : (0 < p)%Q → (0 < p / Zpos n)%Q.
Proof. intros. by apply Qlt_shift_div_l. Qed.

Instance Q_eq_dec : EqDecision Q.
Proof. solve_decision. Defined.
Instance Q_Qeq_dec : RelDecision Qeq.
Proof. intros pq; apply Qeq_dec. Defined.
Instance Q_lt_dec : RelDecision Qlt.
Proof. refine (λ p q, cast_if (Qlt_le_dec p q)); auto using Qle_not_lt. Defined.

Fixpoint Pos_to_pred_nat (p : positive) : nat :=
  match p with
  | xH => 0
  | xO p => S (2 * Pos_to_pred_nat p)
  | xI p => S (S (2 * Pos_to_pred_nat p))
  end.

Definition Q_to_Qp (q : Q) : Qp :=
  match Qred q with
  | Qmake (Zpos n) 1 => Nat.iter (Pos_to_pred_nat n) (λ n, 1 + n) 1
  | Qmake (Zpos n) d => Nat.iter (Pos_to_pred_nat n) (λ n, 1 + n) 1 / d
  | _ => 1 (* dummy *)
  end%Qp.
Arguments Q_to_Qp !_ /.
Arguments Pos.mul !_ !_ /.
Local Arguments Q_to_Qp : simpl never.

Lemma Q_to_Qp_le_0 q : ¬(0 < q)%Q → Q_to_Qp q = 1%Qp.
Proof.
  destruct q as [[|n|n] d]=> //=.
  rewrite /Q_to_Qp /Qred /=. by destruct (Pos.ggcd _ _) as [? [??]].
Qed.
Lemma Q_to_Qp_1 : Q_to_Qp 1 = 1%Qp.
Proof. done. Qed.

Lemma Pos_to_pred_Q_spec p :
  Nat.iter (Pos_to_pred_nat p) (λ n, 1 + n)%Qp 1%Qp == Qmake (Z.pos p) 1.
Proof.
  cut (∀ q,
    Nat.iter (Pos_to_pred_nat p) (λ n, 1 + n)%Qp q == Qmake (Z.pos p) 1 + q - 1).
  { intros ->. rewrite /Qeq /=; lia. }
  induction p as [p IH|p IH|]=> q' //=.
  - rewrite !Qred_correct !Nat_iter_add !IH /=. rewrite /Qeq /=; lia.
  - rewrite !Qred_correct !Nat_iter_add !IH /=. rewrite /Qeq /=; lia.
  - rewrite /Qeq /=; lia.
Qed.

(* this (Qp_car (Q_to_Qp q)) : Q) == q *)
Lemma Q_of_to_Qp (q : Q) : (0 < q)%Q → (Q_to_Qp q : Q) == q.
Proof.
  rewrite -{1 3}(Qred_correct q) /Q_to_Qp.
  destruct (Qred q) as [[|n|n] d]=> ? //=.
  destruct d as [d|d|]=> //=.
  - by rewrite Pos_to_pred_Q_spec !Qred_correct /Qeq /= Z.mul_1_r.
  - by rewrite Pos_to_pred_Q_spec !Qred_correct /Qeq /= Z.mul_1_r.
  - by rewrite Pos_to_pred_Q_spec.
Qed.

(* Q_to_Qp (this (Qp_car q) : Q) = q *)
Lemma Q_to_of_Qp (q : Qp) : Q_to_Qp (q : Q) = q.
Proof. apply Qp_eq, Qc_is_canon. destruct q as [n d]. by rewrite Q_of_to_Qp. Qed.

Instance Q_to_Qp_proper : Proper (Qeq ==> (=)) Q_to_Qp.
Proof. rewrite /Q_to_Qp. by intros p p' ->%Qred_complete. Qed.

Lemma Q_to_Qp_plus p q :
  (0 < p)%Q → (0 < q)%Q → Q_to_Qp (p + q) = (Q_to_Qp p + Q_to_Qp q)%Qp.
Proof.
  intros. assert (0 < p + q)%Q by eauto using Q_plus_nonneg.
  apply Qp_eq, Qc_is_canon. by rewrite /= Qred_correct !Q_of_to_Qp.
Qed.

Lemma Q_to_Qp_div q p :
  (0 < q)%Q → Q_to_Qp (q / Zpos p) = (Q_to_Qp q / p)%Qp.
Proof.
  intros. assert (0 < q / Z.pos p)%Q by eauto using Q_div_nonneg.
  apply Qp_eq, Qc_is_canon. by rewrite /= !Qred_correct !Q_of_to_Qp.
Qed.
