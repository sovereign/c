From iris.algebra Require Import cmra auth gmap frac agree.
From iris.bi.lib Require Import fractional.
From iris.base_logic.lib Require Export own.
From iris.proofmode Require Import tactics.
From iris_c.lib Require Import list.
Set Default Proof Using "Type".
Local Open Scope nat_scope.

Inductive lvl :=
  | LLvl : lvl
  | ULvl : lvl.
Canonical Structure lvlC := leibnizC lvl.

Instance lvl_EqDecision : EqDecision lvl.
Proof. solve_decision. Defined.

Instance lvl_op : Op lvl := λ lv1 lv2,
  match lv1, lv2 with
  | ULvl,_ => ULvl
  | _,ULvl => ULvl
  | _,_ => LLvl
  end.
Arguments lvl_op !_ !_ /.
Instance lvl_valid : Valid lvl := λ _, True.
Instance lvl_unit : Unit lvl := LLvl.
Instance lvl_pcore : PCore lvl := λ _, Some LLvl.
Lemma lvl_included lv1 lv2 :
  lv1 ≼ lv2 ↔ match lv2 with
              | ULvl => True
              | LLvl => if lv1 is LLvl then True else False
              end.
Proof.
  split.
  - intros [[] ->%leibniz_equiv]; by destruct lv1.
  - exists lv2. by destruct lv1, lv2.
Qed.

Lemma lvl_idemp (lv : lvl) : lv ⋅ lv = lv.
Proof. by destruct lv. Qed.

Lemma lvl_ra_mixin : RAMixin lvl.
Proof.
  apply ra_total_mixin; try by eauto.
  - solve_proper.
  - by intros [] [] [].
  - by intros [] [].
  - by intros [].
  - intros lv1 lv2 ?%lvl_included.
    apply lvl_included. destruct lv1, lv2; compute; firstorder.
Qed.

Canonical Structure lvlR : cmraT := discreteR lvl lvl_ra_mixin.
Global Instance lvl_cmra_discrete : CmraDiscrete lvlR.
Proof. apply discrete_cmra_discrete. Qed.

Lemma lvl_ucmra_mixin : UcmraMixin lvl.
Proof. split; try (apply _ || done). by intros []. Qed.
Canonical Structure lvlUR : ucmraT := UcmraT lvl lvl_ucmra_mixin.

(** Here we use a Primitive Record to get an eta-rule for cloc
See: https://coq.inria.fr/refman/language/gallina-extensions.html#primitive-record-types
*)
Set Primitive Projections.
Record cloc := CLoc {
  cloc_base : loc;
  cloc_offset : Z
}.
Unset Primitive Projections.
Add Printing Constructor cloc.

Instance cloc_eq_dec : EqDecision cloc | 0.
Proof. solve_decision. Qed.
Instance cloc_countable : Countable cloc | 0.
Proof.
  apply inj_countable' with
      (f:=(λ x, (cloc_base x, cloc_offset x)))
      (g:=λ '(l,n), CLoc l n).
  reflexivity. (* This line wouldn't work if we were not using primitive projections *)
Qed.
Instance cloc_inhabited : Inhabited cloc | 0 :=
  populate (CLoc inhabitant inhabitant).

(* Auth (CLoc -> (Q * Level)) *)
Definition locking_heapUR : ucmraT :=
  gmapUR cloc (prodR (prodR fracR lvlUR) (agreeR valC)).
Definition heap_block_infoUR : ucmraT :=
  gmapUR loc (agreeR (prodC boolC natC)).

(** The CMRA we need. *)
Class locking_heapG (Σ : gFunctors) := LHeapG {
  lheap_inG :> inG Σ (authR locking_heapUR);
  lheap_block_info_inG :> inG Σ (authR heap_block_infoUR);
  lheap_name : gname;
  lheap_block_info_name : gname
}.
Class locking_heapPreG (Σ : gFunctors) := {
  lheap_preG_inG :> inG Σ (authR locking_heapUR);
  lheap_preG_block_info_inG :> inG Σ (authR heap_block_infoUR);
}.

Definition locking_heapΣ : gFunctors :=
  #[GFunctor (authR locking_heapUR); GFunctor (authR heap_block_infoUR)].

Instance subG_heapPreG {Σ} :
  subG locking_heapΣ Σ → locking_heapPreG Σ.
Proof. solve_inG. Qed.

Section definitions.
  Context `{hG : locking_heapG Σ, !heapG Σ}.

  Definition to_locking_heap (σ : gmap cloc (lvl * val)) : locking_heapUR :=
    fmap (λ '(lv,v), (1%Qp, lv, to_agree v)) σ.

  Inductive heap_block :=
    (* true = alloc, false = local *)
    | ActiveBlock : bool → list (lvl * val) → heap_block
    | FreedBlock : bool → nat → heap_block.
  Definition heap_block_info (b : heap_block) : bool * nat :=
    match b with
    | ActiveBlock b lvvs => (b,length lvvs)
    | FreedBlock b n => (b,n)
    end.
  Global Instance maybe_active_block : Maybe2 ActiveBlock := λ b,
    match b with ActiveBlock k lvvs => Some (k,lvvs) | _ => None end.

  Definition to_heap_block_info (τ : gmap loc heap_block) : heap_block_infoUR :=
   to_agree ∘ heap_block_info <$> τ.

  Definition heap_blocks_lookup (τ : gmap loc heap_block) (cl : cloc) : option (lvl * val) :=
    b ← τ !! cloc_base cl;
    ''(_, lvvs) ← maybe2 ActiveBlock b;
    lvvs !! Z.to_nat (cloc_offset cl).

  (* σ^{-1}(L) *)
  Definition locked_locs (σ : gmap cloc (lvl * val)) : gset cloc :=
    dom _ (filter (λ x, x.2.1 = LLvl) σ).

  (** Pointer arithmetic *)
  Definition cloc_lt (p q : cloc) : bool :=
    bool_decide (cloc_base p = cloc_base q)
    && bool_decide (cloc_offset p < cloc_offset q)%Z.
  Definition cloc_plus (cl : cloc) (i : Z) : cloc :=
    CLoc (cloc_base cl) (i + cloc_offset cl).

  Definition cloc_to_val (cl : cloc) : val :=
    locked (SOMEV (#(cloc_base cl), #(cloc_offset cl)))%V.

  Definition cloc_of_val (v : val) : option cloc :=
    match v return option cloc with
    | SOMEV (LitV (LitLoc l), LitV (LitInt i))%V => Some (CLoc l i)
    | _ => None
    end.

  Definition full_locking_heap (σ : gmap cloc (lvl * val)) :=
    (∃ τ : gmap loc heap_block,
      ⌜ map_Forall (λ cl _, 0 ≤ cloc_offset cl)%Z σ ∧
        ∀ cl, (0 ≤ cloc_offset cl)%Z →
          σ !! cl = heap_blocks_lookup τ cl ⌝ ∧
      own lheap_name (● (to_locking_heap σ)) ∗
      own lheap_block_info_name (● (to_heap_block_info τ)) ∗
      [∗ map] l↦b ∈ τ, match b with
                       | ActiveBlock k lvvs => ∃ lv,
                          l ↦ SOMEV (#k,lv) ∧
                          ⌜ is_list lv (snd <$> lvvs) ⌝
                       | FreedBlock _ _ => l ↦ NONEV
                       end)%I.

  Definition mapsto_def (cl : cloc) (lv : lvl) (q : frac) (v: val) : iProp Σ :=
    (∃ lv',
      ⌜lv ≼ lv'⌝ ∧
      ⌜ 0 ≤ cloc_offset cl ⌝%Z ∧
      own lheap_name (◯ {[ cl := (q, lv', to_agree v) ]}))%I.
  Definition mapsto_aux : seal (@mapsto_def). by eexists. Qed.
  Definition mapsto := unseal mapsto_aux.
  Definition mapsto_eq : @mapsto = @mapsto_def := seal_eq mapsto_aux.

  Definition block_info_def (cl : cloc) (k : bool) (n : nat) : iProp Σ :=
    (⌜ cloc_offset cl = 0 ⌝ ∧ ⌜ n ≠ 0 ⌝ ∧
     own lheap_block_info_name (◯ {[ cloc_base cl := to_agree (k,n) ]}))%I.
  Definition block_info_aux : seal (@block_info_def). by eexists. Qed.
  Definition block_info := unseal block_info_aux.
  Definition block_info_eq : @block_info = @block_info_def := seal_eq block_info_aux.

  Definition mapsto_list (cl : cloc) (q : frac) (vs : list val) : iProp Σ :=
    ([∗ list] i ↦ v ∈ vs, mapsto (cloc_plus cl i) ULvl q v)%I.
End definitions.

Typeclasses Opaque mapsto_list.

Infix "+∗" := (cloc_plus) (at level 50) : stdpp_scope.

Notation "cl ↦C[ lv ]{ q } v" :=
  (mapsto cl lv q%Qp v%V)
  (at level 20, lv, q at level 50, format "cl  ↦C[ lv ]{ q }  v") : bi_scope.
Notation "cl ↦C[ lv ] v" := (cl ↦C[lv]{1} v)%I
  (at level 20, lv at level 50, format "cl  ↦C[ lv ]  v") : bi_scope.
Notation "cl ↦C{ q } v" := (cl ↦C[ULvl]{q} v)%I
  (at level 20, q at level 50, format "cl  ↦C{ q }  v") : bi_scope.
Notation "cl ↦C v" := (cl ↦C[ULvl]{1} v)%I
  (at level 20, format "cl  ↦C  v") : bi_scope.

Notation "cl ↦C{ q }∗ vs" :=
  (mapsto_list cl q vs)%I
  (at level 20, q at level 50, format "cl  ↦C{ q }∗  vs") : bi_scope.
Notation "cl ↦C∗ vs" :=
  (mapsto_list cl 1 vs)%I
  (at level 20, format "cl  ↦C∗  vs") : bi_scope.

Lemma to_locking_heap_valid σ : ✓ to_locking_heap σ.
Proof. intros cl. rewrite lookup_fmap. by destruct (σ !! cl) as [[]|]. Qed.
Lemma to_heap_block_info_valid τ : ✓ to_heap_block_info τ.
Proof. intros l. rewrite lookup_fmap. by destruct (τ !! l) as [[]|]. Qed.

Lemma locking_heap_init `{locking_heapPreG Σ, !heapG Σ} :
  (|==> ∃ _ : locking_heapG Σ, full_locking_heap ∅)%I.
Proof.
  iMod (own_alloc (● to_locking_heap ∅)) as (γ1) "Hh1".
  { apply: auth_auth_valid. exact: to_locking_heap_valid. }
  iMod (own_alloc (● to_heap_block_info ∅)) as (γ2) "Hh2".
  { apply: auth_auth_valid. exact: to_heap_block_info_valid. }
  iModIntro. iExists (LHeapG Σ _ _ γ1 γ2), ∅. iFrame; auto 10.
Qed.

Section properties.
  Context `{locking_heapG Σ, !heapG Σ}.
  Implicit Type σ : gmap cloc (lvl * val).
  Implicit Type lv : lvl.
  Implicit Type l : loc.
  Implicit Type cl : cloc.

  Lemma mapsto_offset_non_neg cl lv q v :
    cl ↦C[lv]{q} v -∗ ⌜ (0 ≤ cloc_offset cl)%Z ⌝.
  Proof. rewrite mapsto_eq /mapsto_def. by iDestruct 1 as (lv1 ??) "_". Qed.

  Lemma mapsto_value_agree lv lv' cl q q' v v' :
    cl ↦C[lv]{q} v -∗ cl ↦C[lv']{q'} v' -∗ ⌜v = v'⌝.
  Proof.
    rewrite mapsto_eq /mapsto_def.
    iDestruct 1 as (lv1 ??) "Hl1". iDestruct 1 as (lv2 ? _) "Hl2".
    iDestruct (own_valid_2 with "Hl1 Hl2") as %Ho%auth_own_valid. iPureIntro.
    revert Ho. rewrite /= op_singleton singleton_valid. by intros [_ ?%agree_op_invL'].
  Qed.

  Lemma mapsto_combine lv lv' cl q q' v :
    cl ↦C[lv]{q} v -∗ cl ↦C[lv']{q'} v -∗ cl ↦C[lv⋅lv']{q+q'} v.
  Proof.
    rewrite mapsto_eq /mapsto_def.
    iDestruct 1 as (lv1 ??) "Hl1". iDestruct 1 as (lv2 ? _) "Hl2".
    iExists (lv1 ⋅ lv2). iSplitR.
    { iPureIntro. by apply: cmra_mono. }
    iCombine "Hl1 Hl2" as "Hl". rewrite frac_op'. by iFrame.
  Qed.

  Global Instance from_sep_mapsto cl lv lv' q q' v :
    FromSep (cl ↦C[lv⋅lv']{q+q'} v) (cl ↦C[lv]{q} v) (cl ↦C[lv']{q'} v).
  Proof.
    rewrite /FromSep. iIntros "[Hl1 Hl2]".
    iApply (mapsto_combine with "Hl1 Hl2").
  Qed.

  Global Instance mapsto_timeless cl lv q v : Timeless (cl ↦C[lv]{q} v).
  Proof. rewrite mapsto_eq /mapsto_def. apply _. Qed.
  Global Instance mapsto_fractional cl lv v : Fractional (λ q, cl ↦C[lv]{q} v)%I.
  Proof.
    intros p q. iSplit.
    - rewrite mapsto_eq /mapsto_def.
      iDestruct 1 as (lv' ??) "Hl". rewrite -(lvl_idemp lv').
      iDestruct "Hl" as "[Hl Hl']". iSplitL "Hl"; eauto 10.
    - iIntros "[H1 H2]". iDestruct (mapsto_combine with "H1 H2") as "H".
      by rewrite lvl_idemp.
  Qed.
  Global Instance mapsto_as_fractional cl q lv v :
    AsFractional (cl ↦C[lv]{q} v) (λ q, cl ↦C[lv]{q} v)%I q.
  Proof. split. done. apply _. Qed.

  Global Instance mapsto_list_timeless cl q vs : Timeless (cl ↦C{q}∗ vs).
  Proof. rewrite /mapsto_list; apply _. Qed.

  Lemma mapsto_downgrade' lv lv' cl q v :
    lv ≼ lv' → cl ↦C[lv']{q} v -∗ cl ↦C[lv]{q} v.
  Proof.
    rewrite mapsto_eq /mapsto_def. iDestruct 1 as (lv'' ?) "Hl".
    iExists lv''. iSplit; eauto. iPureIntro. by transitivity lv'.
  Qed.

  Lemma mapsto_downgrade lv cl q v : cl ↦C{q} v -∗ cl ↦C[lv]{q} v.
  Proof. apply mapsto_downgrade'. by apply lvl_included. Qed.

  Lemma cloc_plus_0 cl : cl +∗ 0 = cl.
  Proof. reflexivity. Qed.
  Lemma cloc_plus_plus cl i j : (cl +∗ i) +∗ j = cl +∗ (i + j).
  Proof. by rewrite /cloc_plus /= Z.add_assoc (Z.add_comm i j). Qed.
  Global Instance cloc_plus_inj cl : Inj (=) (=) (cloc_plus cl).
  Proof. intros i j [=]; lia. Qed.

  Lemma mapsto_list_nil cl q : cl ↦C{q}∗ [] ⊣⊢ True.
  Proof. done. Qed.
  Lemma mapsto_list_singleton cl q v : cl ↦C{q}∗ [v] ⊣⊢ cl ↦C{q} v.
  Proof. by rewrite /mapsto_list big_sepL_singleton. Qed.
  Lemma mapsto_list_app cl q vs1 vs2 :
    cl ↦C{q}∗ (vs1 ++ vs2) ⊣⊢ cl ↦C{q}∗ vs1 ∗ (cl +∗ length vs1) ↦C{q}∗ vs2.
  Proof.
    rewrite /mapsto_list /= big_sepL_app.
    by setoid_rewrite cloc_plus_plus; setoid_rewrite Nat2Z.inj_add.
  Qed.
  Lemma mapsto_list_cons cl q v vs :
    cl ↦C{q}∗ (v :: vs) ⊣⊢ cl ↦C{q} v ∗ (cl +∗ 1) ↦C{q}∗ vs.
  Proof. by rewrite (mapsto_list_app _ _ [_]) mapsto_list_singleton. Qed.

  Global Instance block_info_persistent cl k n : Persistent (block_info cl k n).
  Proof. rewrite block_info_eq. apply _. Qed.
  Global Instance block_info_timeless cl k n : Timeless (block_info cl k n).
  Proof. rewrite block_info_eq. apply _. Qed.

  Lemma cloc_to_val_eq : cloc_to_val = λ cl, SOMEV (#(cloc_base cl), #(cloc_offset cl))%V.
  Proof. rewrite /cloc_to_val. by unlock. Qed.
  Lemma cloc_of_to_val cl : cloc_of_val (cloc_to_val cl) = Some cl.
  Proof. destruct cl. by rewrite cloc_to_val_eq. Qed.
  Lemma cloc_to_of_val v cl : cloc_of_val v = Some cl → cloc_to_val cl = v.
  Proof.
    rewrite /cloc_of_val cloc_to_val_eq=> ?.
    destruct cl; repeat (case_match || simplify_option_eq); auto.
  Qed.
  Global Instance cloc_to_val_inj : Inj (=) (=) cloc_to_val.
  Proof. intros cl1 cl2 Hcl. apply (inj Some). by rewrite -!cloc_of_to_val Hcl. Qed.

  Lemma to_locking_heap_insert σ cl lv v :
    to_locking_heap (<[cl:=(lv,v)]> σ) = <[cl:=(1%Qp, lv, to_agree v)]>(to_locking_heap σ).
  Proof. by rewrite /to_locking_heap fmap_insert. Qed.

  Lemma to_locking_heap_lookup_Some σ li lv v :
    σ !! li = Some (lv,v) → to_locking_heap σ !! li = Some (1%Qp, lv, to_agree v).
  Proof. rewrite /to_locking_heap lookup_fmap => -> //. Qed.
  Lemma to_locking_heap_lookup_None σ li :
    σ !! li = None → to_locking_heap σ !! li = None.
  Proof. rewrite /to_locking_heap lookup_fmap => -> //. Qed.

  Lemma locked_locs_lock σ cl v :
    locked_locs (<[cl:=(LLvl,v)]>σ) = {[cl]} ∪ locked_locs σ.
  Proof.
    rewrite /locked_locs map_filter_insert; last done.
    apply dom_insert_L.
  Qed.

  Lemma locked_locs_delete σ cl :
    locked_locs (delete cl σ) = locked_locs σ ∖ {[cl]}.
  Proof. by rewrite /locked_locs map_filter_delete dom_delete_L. Qed.

  Lemma locked_locs_unlock σ cl v :
    σ !! cl = Some (LLvl,v) →
    locked_locs (<[cl:=(ULvl,v)]>σ) = locked_locs σ ∖ {[cl]}.
  Proof.
    intros Hl. rewrite /locked_locs -dom_delete_L. f_equal.
    apply map_eq=> j. apply option_eq=> x.
    rewrite lookup_delete_Some !map_filter_lookup_Some lookup_insert_Some. naive_solver.
  Qed.

  Lemma locked_locs_alloc_unlocked σ cl v :
    σ !! cl = None →
    locked_locs (<[cl:=(ULvl,v)]>σ) = locked_locs σ.
  Proof.
    intros Hl. rewrite /locked_locs. f_equal.
    apply map_eq => j. apply option_eq=> x.
    rewrite !map_filter_lookup_Some lookup_insert_Some. naive_solver.
  Qed.

  Lemma heap_singleton_included σ cl lv q v :
    {[cl := (q, lv, to_agree v)]} ≼ to_locking_heap σ →
    ∃ lv', lv ≼ lv' ∧ σ !! cl = Some (lv', v).
  Proof.
    rewrite singleton_included=> -[[[q' lv'] av] []].
    rewrite /to_gen_heap lookup_fmap fmap_Some_equiv => -[[lv'' av'] [Hl [/= -> ->]]].
    move=> /Some_pair_included_total_2 [/Some_pair_included_total_2] [_] Hxb'.
    move=> /to_agree_included /leibniz_equiv_iff=> ->. by exists lv''.
  Qed.

  Lemma block_info_singleton_included τ l k n :
    {[l := to_agree (k,n)]} ≼ to_heap_block_info τ →
    ∃ b, τ !! l = Some b ∧ heap_block_info b = (k,n).
  Proof.
    rewrite singleton_included=> -[mn].
    rewrite /to_heap_block_info lookup_fmap fmap_Some_equiv=> -[[b [-> ->]]] /=.
    move=> /Some_included_total /to_agree_included /leibniz_equiv_iff; eauto.
  Qed.

  Lemma full_locking_heap_unlocked_value σ cl v q :
    cl ↦C{q} v -∗ full_locking_heap σ -∗ ⌜σ !! cl = Some (ULvl,v)⌝.
  Proof.
    rewrite mapsto_eq /mapsto_def /full_locking_heap.
    iDestruct 1 as ([] []%lvl_included _) "Hl".
    iDestruct 1 as (τ Hτ) "[Hfull Hmap]".
    iDestruct (own_valid_2 with "Hfull Hl")
      as %[[[] [[]%lvl_included Hl]]%heap_singleton_included _]%auth_valid_discrete_2; auto.
  Qed.

  Lemma full_locking_heap_unlocked σ cl v q :
    cl ↦C{q} v -∗ full_locking_heap σ -∗ ⌜cl ∉ locked_locs σ⌝.
  Proof.
    iIntros "Hcl Hσ".
    iDestruct (full_locking_heap_unlocked_value with "Hcl Hσ") as %Hcl.
    iPureIntro.
    rewrite /locked_locs.
    apply not_elem_of_dom, map_filter_lookup_None.
    right. intros [??]. by rewrite Hcl=>[= <- <-].
  Qed.

  Lemma full_locking_heap_locked_present σ cl v q :
    cl ↦C[LLvl]{q} v -∗ full_locking_heap σ -∗ ⌜is_Some (σ !! cl)⌝.
  Proof.
    rewrite mapsto_eq /mapsto_def /full_locking_heap.
    iDestruct 1 as (lv _ _) "Hl". iDestruct 1 as (τ Hτ) "[Hfull Hmap]".
    iDestruct (own_valid_2 with "Hfull Hl")
      as %[[y [? ?]]%heap_singleton_included _]%auth_valid_discrete_2; eauto.
  Qed.

  Lemma full_locking_heap_load_upd cl lv q v σ :
    full_locking_heap σ -∗ cl ↦C[lv]{q} v ==∗ ∃ (k : bool) ll vs,
      ⌜ is_list ll vs ⌝ ∧
      ⌜ vs !! Z.to_nat (cloc_offset cl) = Some v ⌝ ∗
      cloc_base cl ↦ SOMEV (#k,ll) ∗
      (cloc_base cl ↦ SOMEV (#k,ll) -∗ full_locking_heap σ ∗ cl ↦C[lv]{q} v).
  Proof.
    iDestruct 1 as (τ [Hnat Hτ]) "(Hσ & Hτ & H)".
    rewrite mapsto_eq /mapsto_def; iDestruct 1 as (lv' ??) "Hl".
    iDestruct (own_valid_2 with "Hσ Hl")
      as %[(lv''&?&Hσ)%heap_singleton_included _]%auth_valid_discrete_2.
    move: (Hσ). rewrite Hτ; last by eauto.
    intros ([k lvvs|?]&?&Hi)%bind_Some; simplify_eq/=.
    iDestruct (big_sepM_lookup_acc with "H") as "[H Hclose]"; first done.
    iDestruct "H" as (ll) "[Hll %]". iModIntro. iExists k, ll, lvvs.*2.
    repeat iSplit; eauto.
    { iPureIntro. by rewrite list_lookup_fmap Hi. }
    iIntros "{$Hll} Hll". iDestruct ("Hclose" with "[Hll]") as "H"; eauto.
    iSplitL "Hσ Hτ H"; first (iExists τ; by eauto with iFrame).
    iExists lv'. auto.
  Qed.

  Lemma full_locking_heap_store_upd cl lv v σ :
    full_locking_heap σ -∗ cl ↦C[lv] v ==∗ ∃ (k : bool) ll vs,
      ⌜ is_list ll vs ⌝ ∧
      ⌜ vs !! Z.to_nat (cloc_offset cl) = Some v ⌝ ∗
      cloc_base cl ↦ SOMEV (#k, ll) ∗
      (∀ ll' lv' v',
        ⌜ is_list ll' (<[Z.to_nat (cloc_offset cl):=v']>vs) ⌝ -∗
        cloc_base cl ↦ SOMEV (#k, ll') ==∗
        full_locking_heap (<[cl:=(lv',v')]>σ) ∗ cl ↦C[lv'] v').
  Proof.
    iDestruct 1 as (τ [Hnat Hτ]) "(Hσ & Hτ & H)".
    rewrite mapsto_eq /mapsto_def; iDestruct 1 as (lv' ??) "Hl".
    iDestruct (own_valid_2 with "Hσ Hl")
      as %[(lv''&?&Hσ)%heap_singleton_included _]%auth_valid_discrete_2.
    move: (Hσ). rewrite Hτ; last by eauto.
    intros ([k lvvs|?]&Hτl&Hi)%bind_Some; simplify_eq/=.
    iDestruct (big_sepM_delete with "H") as "[Hcl H]"; first done.
    iDestruct "Hcl" as (ll) "[Hll %]". iModIntro. iExists k, ll, lvvs.*2.
    repeat iSplit; eauto.
    { iPureIntro. by rewrite list_lookup_fmap Hi. }
    iIntros "{$Hll}" (ll' lv''' v' ?) "Hll".
    iMod (own_update_2 with "Hσ Hl") as "[Hσ Hl]".
    { by eapply auth_update, singleton_local_update,
        (exclusive_local_update _ (1%Qp, lv''', to_agree v'));
        first by apply to_locking_heap_lookup_Some. }
    iModIntro. iSplitL "Hσ Hτ H Hll"; last auto.
    iExists (<[cloc_base cl:=
      ActiveBlock k (<[Z.to_nat (cloc_offset cl):=(lv''',v')]> lvvs)]>τ).
    rewrite to_locking_heap_insert. iFrame "Hσ". iSplit.
    { iPureIntro. split; [by apply map_Forall_insert_2; eauto|].
      destruct cl as [l i]=> -[l' i'] ?; simpl in *.
      assert (0 ≤ i)%Z by (by eapply (Hnat (CLoc l i))).
      destruct (decide (l' = l)) as [->|?].
      { destruct (decide (i' = i)) as [->|?].
        - rewrite /heap_blocks_lookup /= !lookup_insert /= list_lookup_insert //.
          by eapply lookup_lt_Some.
        - rewrite /heap_blocks_lookup lookup_insert /=.
          rewrite list_lookup_insert_ne ?Z2Nat.inj_iff //=.
          rewrite lookup_insert_ne; last congruence.
          by rewrite Hτ //= /heap_blocks_lookup /= Hτl. }
      rewrite /heap_blocks_lookup /= !lookup_insert_ne; [|congruence..].
      by rewrite Hτ. }
    iSplitL "Hτ".
    { rewrite /to_heap_block_info fmap_insert insert_id //.
      by rewrite lookup_fmap Hτl /= insert_length. }
    rewrite -insert_delete.
    iApply (big_sepM_insert with "[$H Hll]"); first by rewrite lookup_delete.
    iExists ll'. iFrame. rewrite list_fmap_insert /=. auto.
  Qed.

  Definition alloc_heap (σ : gmap cloc (lvl * val)) (l : loc) :
      list val → nat → gmap cloc (lvl * val) :=
    foldr (λ v go (i : nat), <[CLoc l i:=(ULvl,v)]> (go (S i))) (λ _, σ).

  Lemma alloc_heap_None σ l vs j1 j2 :
    (∀ i, σ !! CLoc l i = None) →
    j2 < j1 → alloc_heap σ l vs j1 !! (CLoc l j2) = None.
  Proof.
    intros Hσi. revert j1 j2. induction vs as [|v' vs IH]=> j1 j2 ?; csimpl.
    { by rewrite Hσi. }
    rewrite lookup_insert_ne; last (intros [=]; lia).
    by rewrite IH; last lia.
  Qed.

  Lemma alloc_heap_lookup σ l vs (i j : nat) :
    (∀ i, σ !! CLoc l i = None) →
    alloc_heap σ l vs j !! CLoc l (j + i) = ((ULvl,) <$> vs) !! i.
  Proof.
    intros Hσi. revert i j. induction vs as [|v vs IH]=> i j; csimpl.
    { by rewrite Hσi. }
    destruct i as [|i]; simpl.
    { by rewrite Z.add_0_r lookup_insert. }
    rewrite Nat2Z.inj_succ Z.add_succ_r -Z.add_succ_l -Nat2Z.inj_succ.
    rewrite lookup_insert_ne; last (intros [=]; lia).
    apply (IH i (S j)).
  Qed.

  Lemma alloc_heap_lookup_ne σ l l' vs i j :
    l ≠ l' →
    alloc_heap σ l vs j !! CLoc l' i = σ !! CLoc l' i.
  Proof.
    intros Hl. revert i j. induction vs as [|v vs IH]=> i j //; csimpl.
    by rewrite lookup_insert_ne; last congruence.
  Qed.

  Lemma locked_locs_alloc_heap σ l vs j :
    (∀ i, σ !! CLoc l i = None) →
    locked_locs (alloc_heap σ l vs j) = locked_locs σ.
  Proof.
    intros ?. revert j. induction vs as [|v vs IH]=> j //=.
    rewrite locked_locs_alloc_unlocked // alloc_heap_None //; lia.
  Qed.

  Lemma full_locking_heap_alloc_upd l (k : bool) ll vs σ :
    is_list ll vs → vs ≠ [] →
    full_locking_heap σ -∗ l ↦ SOMEV (#k,ll) ==∗
    ⌜ ∀ i, σ !! CLoc l i = None ⌝ ∧
    full_locking_heap (alloc_heap σ l vs O) ∗
    block_info (CLoc l 0) k (length vs) ∗ CLoc l 0 ↦C∗ vs.
  Proof.
    intros Hll Hnil. iDestruct 1 as (τ [Hnat Hτ]) "(Hσ & Hτ &H)". iIntros "Hll".
    iAssert ⌜ τ !! l = None ⌝%I as %Hτi.
    { rewrite eq_None_not_Some. iIntros ([b ?]).
      iDestruct (big_sepM_lookup with "H") as "H"; first done.
      destruct b as [lvv|n].
      - iDestruct "H" as (lv) "[Hll' _]".
        by iDestruct (mapsto_valid_2 with "Hll Hll'") as %[].
      - by iDestruct (mapsto_valid_2 with "Hll H") as %[]. }
    iAssert ⌜ ∀ i, σ !! CLoc l i = None ⌝%I as %Hσi.
    { iIntros (i) "!%". apply eq_None_not_Some=> -[[lv v] Hi].
      move: (Hi). rewrite Hτ; last by eauto.
      by rewrite /heap_blocks_lookup /= Hτi. }
    iAssert (|==> own lheap_name (● to_locking_heap (alloc_heap σ l vs 0)) ∗
      [∗ list] i↦v ∈ vs, own lheap_name (◯ {[ CLoc l (i+0) := (1%Qp, ULvl,to_agree v) ]}))%I
      with "[Hσ]" as ">[Hσ Hl]".
    { clear Hll Hnil. change 0%Z with (Z.of_nat 0). generalize 0=> j.
      iInduction vs as [|v vs] "IH" forall (j); simpl; first by iFrame.
      iMod ("IH" $! (S j) with "Hσ") as "[Hσ Hls]".
      iMod (own_update with "Hσ") as "[Hσ Hl]".
      { eapply auth_update_alloc,
          (alloc_singleton_local_update _ (CLoc l j)
                                        (1%Qp, ULvl, to_agree v)); try done.
        apply to_locking_heap_lookup_None. rewrite alloc_heap_None //. lia. }
      iModIntro.
      rewrite -to_locking_heap_insert /=. iFrame "Hσ Hl".
      iApply (big_sepL_impl with "Hls"); iIntros "!>" (k' w _) "?".
      by rewrite Nat2Z.inj_succ Z.add_succ_r -Z.add_succ_l -Nat2Z.inj_succ. }
    iMod (own_update with "Hτ") as "[Hτ Hinfo]".
    { apply auth_update_alloc,
        (alloc_singleton_local_update _ l (to_agree (k, length vs)))=> //.
      by rewrite /to_heap_block_info lookup_fmap Hτi. }
    iModIntro. iSplit; first done. iSplitL "Hσ H Hτ Hll".
    { iExists (<[l:=ActiveBlock k ((ULvl,) <$> vs)]> τ). iFrame "Hσ". iSplit.
      { iPureIntro. split.
        { clear -Hnat. generalize 0. induction vs as [|v vs IH]=> j //=.
          apply map_Forall_insert_2; simpl; eauto with lia. }
        move=> -[l' i] /= ? . destruct (decide (l' = l)) as [->|].
        - rewrite /heap_blocks_lookup /= lookup_insert /=.
          rewrite -{1}(Z2Nat.id i) //. by apply (alloc_heap_lookup _ _ _ _ 0).
        - rewrite /heap_blocks_lookup /= lookup_insert_ne //=.
          by rewrite alloc_heap_lookup_ne // Hτ. }
      iSplitL "Hτ".
      { by rewrite /to_heap_block_info /= !fmap_insert /= fmap_length. }
      iApply (big_sepM_insert with "[$H Hll]"); first done.
      iExists _. iFrame "Hll".
      by rewrite -list_fmap_compose (list_fmap_ext _ id _ vs) ?list_fmap_id. }
    iSplitL "Hinfo".
    { rewrite block_info_eq /block_info_def. eauto 10 using nil_length_inv. }
    rewrite /mapsto_list.
    iApply (big_sepL_impl with "Hl"); iIntros "!>" (i v _) "Hl".
    rewrite mapsto_eq /mapsto_def /=. eauto 10 with lia.
  Qed.

  Fixpoint free_heap (σ : gmap cloc (lvl * val)) (l : loc)
      (n : nat) : gmap cloc (lvl * val) :=
    match n with
    | 0 => σ
    | S n => delete (CLoc l n) (free_heap σ l n)
    end.

  Lemma locked_locs_free_heap σ cl l (n : nat) :
    cl ∈ locked_locs σ →
    ¬(cloc_base cl = l ∧ (0 ≤ cloc_offset cl < n)%Z) →
    cl ∈ locked_locs (free_heap σ l n).
  Proof.
    induction n as [|n IH]; simpl; intros; auto.
    rewrite locked_locs_delete. set_solver by eauto with lia.
  Qed.

  Lemma free_heap_lookup σ l (n : nat) cl :
    cloc_base cl = l → (0 ≤ cloc_offset cl < n)%Z →
    free_heap σ l n !! cl = None.
  Proof.
    intros. induction n as [|n IH]; simpl; intros; auto with lia.
    destruct (decide (cloc_offset cl = n)).
    - destruct cl as [l' j]; simplify_eq/=. by rewrite lookup_delete.
    - rewrite lookup_delete_ne; last (destruct cl; naive_solver lia). eauto with lia.
  Qed.

  Lemma free_heap_lookup_ge σ l (n : nat) cl :
    cloc_base cl = l → (n ≤ cloc_offset cl)%Z →
    free_heap σ l n !! cl = σ !! cl.
  Proof.
    intros. induction n as [|n IH]; simpl; intros; auto with lia.
    rewrite lookup_delete_ne; last (destruct cl; naive_solver lia). eauto with lia.
  Qed.

  Lemma free_heap_lookup_ne σ l n cl :
    cloc_base cl ≠ l → free_heap σ l n !! cl = σ !! cl.
  Proof.
    intros. induction n as [|n IH]=> //=.
    by rewrite lookup_delete_ne; last (destruct cl; naive_solver).
  Qed.

  Lemma full_locking_heap_free_upd cl k vs σ :
    full_locking_heap σ -∗
    block_info cl k (length vs) -∗
    cl ↦C∗ vs ==∗ ∃ ll,
      ⌜ cloc_offset cl = 0 ⌝ ∧
      ⌜ is_list ll vs ⌝ ∧
      cloc_base cl ↦ SOMEV (#k, ll) ∗
      (cloc_base cl ↦ NONEV -∗
        full_locking_heap (free_heap σ (cloc_base cl) (length vs))).
  Proof.
    iDestruct 1 as (τ [Hnat Hτ]) "(Hσ & Hτ &H)".
    rewrite block_info_eq /block_info_def; iIntros "(Hoff & % & Hinfo) Hcl".
    iDestruct "Hoff" as %Hoff.
    iDestruct (own_valid_2 with "Hτ Hinfo")
      as %[(b&Hb&?)%block_info_singleton_included _]%auth_valid_discrete_2.
    iDestruct (big_sepM_delete with "H") as "[Hb H]"; first done.
    destruct b as [k' lvvs|k' n]; simplify_eq/=; last first.
    { destruct vs as [|v vs]; simplify_eq/=.
      rewrite /mapsto_list mapsto_eq /mapsto_def; iDestruct "Hcl" as "[Hcl _]".
      iDestruct "Hcl" as (lv' _ _) "Hcl". rewrite cloc_plus_0.
      iDestruct (own_valid_2 with "Hσ Hcl")
        as %[(lv''&_&Hcl)%heap_singleton_included ?]%auth_valid_discrete_2.
      contradict Hcl. rewrite Hτ ?Hoff //. by rewrite /heap_blocks_lookup Hb. }
    iDestruct "Hb" as (ll) "[Hll %]".
    iAssert ⌜ vs = lvvs.*2 ⌝%I as %->.
    { iApply bi.pure_mono; [apply list_eq_same_length; by rewrite ?fmap_length|].
      iIntros (i v1 v2 _ ? Hi). rewrite /mapsto_list mapsto_eq /mapsto_def.
      iDestruct (big_sepL_lookup with "Hcl") as "Hcl"; first done.
      iDestruct "Hcl" as (lv' _ _) "Hcl".
      iDestruct (own_valid_2 with "Hσ Hcl")
        as %[(lv''&_&Hcl)%heap_singleton_included ?]%auth_valid_discrete_2.
      iPureIntro; move: Hcl.
      rewrite Hτ /heap_blocks_lookup /cloc_plus /= ?Hoff; last lia.
      rewrite Hb /= Z.add_0_r Nat2Z.id.
      move: Hi. by rewrite list_lookup_fmap fmap_Some=> -[[??] [-> ->]] []. }
    iAssert (own lheap_name (● to_locking_heap
      (free_heap σ (cloc_base cl) (length lvvs.*2))))%I with "[> Hcl Hσ]" as "Hσ".
    { generalize (lvvs.*2)=> vs.
      iInduction vs as [|v vs] "IH" using rev_ind; simpl; auto.
      rewrite mapsto_list_app mapsto_list_singleton mapsto_eq /mapsto_def.
      iDestruct "Hcl" as "[Hcl Hcl']"; iDestruct "Hcl'" as (lv' _ _) "Hcl'".
      iMod ("IH" with "Hcl Hσ") as "Hσ". iApply (own_update_2 with "Hσ Hcl'").
      rewrite /cloc_plus Hoff Z.add_0_r.
      rewrite app_length Nat.add_comm /= /to_locking_heap fmap_delete.
      apply auth_update_dealloc, delete_singleton_local_update, _. }
    rewrite fmap_length.
    iModIntro. iExists _. do 2 (iSplit; first done). iIntros "{$Hll} Hll".
    iExists (<[cloc_base cl:=FreedBlock k (length lvvs.*2)]> τ). iSplit.
    { iPureIntro; split.
      { generalize (length lvvs); intros n.
        induction n; simpl; eauto. by apply map_Forall_delete. }
      intros cl' ?. destruct (decide (cloc_base cl = cloc_base cl')) as [Hcl|].
      - rewrite /heap_blocks_lookup Hcl lookup_insert /=.
        destruct (Z_lt_le_dec (cloc_offset cl') (length lvvs)%Z).
        + by rewrite free_heap_lookup.
        + rewrite free_heap_lookup_ge // Hτ //.
          rewrite /heap_blocks_lookup -Hcl Hb /=.
          apply lookup_ge_None_2, Nat2Z.inj_le. by rewrite Z2Nat.id.
      - rewrite free_heap_lookup_ne // Hτ //.
        rewrite /heap_blocks_lookup lookup_insert_ne //. }
    iFrame "Hσ". iSplitL "Hτ".
    { rewrite /to_heap_block_info fmap_insert insert_id //.
      by rewrite lookup_fmap Hb /= fmap_length. }
    rewrite -insert_delete.
    iApply big_sepM_insert; first by rewrite lookup_delete. iFrame.
  Qed.

  Lemma full_locking_heap_unlock cl v lv q σ :
    full_locking_heap σ -∗ cl ↦C[lv]{q} v ==∗
    full_locking_heap (<[cl:=(ULvl,v)]>σ) ∗ cl ↦C{q} v.
  Proof.
    iDestruct 1 as (τ [Hnat Hτ]) "(Hσ & Hτ &H)".
    rewrite mapsto_eq /mapsto_def; iDestruct 1 as (lv' ??) "Hl".
    iDestruct (own_valid_2 with "Hσ Hl")
      as %[(lv''&?&Hσ)%heap_singleton_included _]%auth_valid_discrete_2.
    move: (Hσ); rewrite Hτ; last by eauto.
    intros ([k lvvs|?]&Hτl&Hi)%bind_Some; simplify_eq/=.
    iDestruct (big_sepM_delete with "H") as "[Hcl H]"; first done.
    iDestruct "Hcl" as (ll) "[Hll %]".
    iMod (own_update_2 with "Hσ Hl") as "[Hσ Hl]".
    { eapply auth_update, singleton_local_update, prod_local_update_1,
        prod_local_update_2, (local_update_unital_discrete _ _ ULvl ULvl);
        first by apply to_locking_heap_lookup_Some.
      intros h Hh. fold_leibniz. intros ->. split; eauto. }
    iModIntro. iSplitR "Hl"; last by eauto with iFrame.
    iExists (<[cloc_base cl:=
      ActiveBlock k (<[Z.to_nat (cloc_offset cl):=(ULvl,v)]> lvvs)]>τ).
    rewrite to_locking_heap_insert. iFrame "Hσ". iSplit.
    { iPureIntro. split; [by apply map_Forall_insert_2; eauto|].
      destruct cl as [l i]=> -[l' i'] ?; simpl in *.
      assert (0 ≤ i)%Z by (by eapply (Hnat (CLoc l i))).
      destruct (decide (l' = l)) as [->|?].
      { destruct (decide (i' = i)) as [->|?].
        - rewrite /heap_blocks_lookup /= !lookup_insert /= list_lookup_insert //.
          by eapply lookup_lt_Some.
        - rewrite /heap_blocks_lookup /= lookup_insert /=.
          rewrite list_lookup_insert_ne ?Z2Nat.inj_iff //=.
          rewrite lookup_insert_ne; last congruence.
          by rewrite Hτ // /heap_blocks_lookup /= Hτl. }
      rewrite /heap_blocks_lookup /= !lookup_insert_ne; [|congruence..].
      by rewrite Hτ. }
    iSplitL "Hτ".
    { rewrite /to_heap_block_info /= fmap_insert insert_id //.
      by rewrite lookup_fmap Hτl /= insert_length. }
    rewrite -insert_delete.
    iApply (big_sepM_insert with "[$H Hll]"); first by rewrite lookup_delete.
    iExists ll. iFrame.
    by rewrite list_fmap_insert /= list_insert_id ?list_lookup_fmap ?Hi.
  Qed.
End properties.

Section proofmode.
  Context `{locking_heapG Σ, !heapG Σ}.

  Global Instance into_pure_mapsto_nil cl q : IntoPure (cl ↦C{q}∗ []) True.
  Proof. done. Qed.
  Global Instance from_pure_mapsto_nil a cl q : FromPure a (cl ↦C{q}∗ []) True | 100.
  Proof. by rewrite /FromPure bi.affinely_if_elim. Qed.

  Global Instance into_sep_mapsto_list_cons cl q vs v vs' :
    IsCons vs v vs' →
    IntoSep (cl ↦C{q}∗ vs) (cl ↦C{q} v) ((cl +∗ 1) ↦C{q}∗ vs').
  Proof. rewrite /IsCons=> ->. by rewrite /IntoSep mapsto_list_cons. Qed.
  Global Instance into_sep_mapsto_list_app cl q vs vs1 vs2 :
    IsApp vs vs1 vs2 →
    IntoSep (cl ↦C{q}∗ vs) (cl ↦C{q}∗ vs1) ((cl +∗ length vs1) ↦C{q}∗ vs2).
  Proof. rewrite /IsApp=> ->. by rewrite /IntoSep mapsto_list_app. Qed.

  Global Instance from_sep_mapsto_list_cons cl q vs v vs' :
    IsCons vs v vs' →
    FromSep (cl ↦C{q}∗ vs) (cl ↦C{q} v) ((cl +∗ 1) ↦C{q}∗ vs').
  Proof. rewrite /IsCons=> ->. by rewrite /FromSep mapsto_list_cons. Qed.
  Global Instance from_sep_mapsto_list_app cl q vs vs1 vs2 :
    IsApp vs vs1 vs2 →
    FromSep (cl ↦C{q}∗ vs) (cl ↦C{q}∗ vs1) ((cl +∗ length vs1) ↦C{q}∗ vs2).
  Proof. rewrite /IsApp=> ->. by rewrite /FromSep mapsto_list_app. Qed.

  Global Instance frame_big_sepL_cons p R Q cl q vs v vs' :
    IsCons vs v vs' →
    Frame p R (cl ↦C{q} v ∗ (cl +∗ 1) ↦C{q}∗ vs') Q →
    Frame p R (cl ↦C{q}∗ vs) Q.
  Proof. rewrite /IsCons=>->. by rewrite /Frame mapsto_list_cons. Qed.
  Global Instance frame_big_sepL_app p R Q cl q vs vs1 vs2 :
    IsApp vs vs1 vs2 →
    Frame p R (cl ↦C{q}∗ vs1 ∗ (cl +∗ length vs1) ↦C{q}∗ vs2) Q →
    Frame p R (cl ↦C{q}∗ vs) Q.
  Proof. rewrite /IsApp=>->. by rewrite /Frame mapsto_list_app. Qed.
End proofmode.
