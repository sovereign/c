From iris.heap_lang Require Export proofmode notation.
From iris.heap_lang Require Import assert.
From iris_c.lib Require Import list.

Definition is_mset `{heapG Σ} (v : val) (X : gset val) : iProp Σ :=
  (∃ (l : loc) hd vs,
    ⌜ v = #l ∧ X = list_to_set vs ∧ is_list hd vs ∧ NoDup vs ⌝ ∗ l ↦ hd)%I.

Definition mset_create : val := λ: <>, ref (lnil #()).

Definition mset_member : val := λ: "x" "xs",
  let: "l" := !"xs" in
  llist_member "x" "l".

Definition mset_add : val := λ: "x" "xs",
  let: "l" := !"xs" in
  assert: (llist_member "x" "l" = #false);;
  "xs" <- lcons "x" "l".

Definition mset_clear : val := λ: "xs",
  "xs" <- lnil #().

Section mset.
  Context `{heapG Σ}.
  Implicit Types x v : val.

  Lemma mset_create_spec :
    {{{ True }}} mset_create #() {{{ x, RET x; is_mset x ∅ }}}.
  Proof.
    iIntros (Φ) "_ HΦ". wp_lam. wp_apply (lnil_spec with "[//]").
    iIntros (hd ?); simplify_eq/=. wp_alloc l as "Hl". iApply "HΦ".
    iExists l, _, []. iFrame "Hl". by rewrite /= NoDup_nil.
  Qed.

  Lemma mset_member_spec x e v X :
    IntoVal e v →
    {{{ is_mset x X }}}
      mset_member e x
    {{{ RET #(bool_decide (v ∈ X)); is_mset x X }}}.
  Proof.
    iIntros (<- Φ) "Hmut HΦ". iDestruct "Hmut" as (l hd vs (->&->&?&?)) "Hl".
    wp_lam; wp_pures. wp_load. wp_let.
    wp_apply (llist_member_spec with "[//]"). iIntros "_".
    rewrite (bool_decide_iff _ (v ∈ vs)); last set_solver.
    iApply "HΦ". iExists l, hd, vs; auto.
  Qed.

  Lemma mset_add_spec x e v X :
    IntoVal e v →
    v ∉ X →
    {{{ is_mset x X }}}
      mset_add e x
    {{{ RET #(); is_mset x ({[v]} ∪ X) }}}.
  Proof.
    iIntros (<- Φ ?) "Hmut HΦ". iDestruct "Hmut" as (l hd vs (->&->&?&?)) "Hl".
    wp_lam. wp_pures. wp_load. wp_let. wp_apply wp_assert.
    wp_apply (llist_member_spec with "[//]"); iIntros "_".
    rewrite bool_decide_false /=; last set_solver. wp_op; iSplit=> //; iIntros "!>".
    wp_seq. wp_apply (lcons_spec with "[//]"); iIntros (hd' ?). wp_store.
    iApply "HΦ". iExists l, hd', (v :: vs). iFrame "Hl".
    rewrite NoDup_cons. iPureIntro; set_solver.
  Qed.

  Lemma mset_clear_spec x X :
    {{{ is_mset x X }}} mset_clear x {{{ RET #(); is_mset x ∅}}}.
  Proof.
    iIntros (Φ) "Hmut HΦ". iDestruct "Hmut" as (l hd vs (->&->&?&?)) "Hl".
    wp_lam. wp_apply (lnil_spec with "[//]").
    iIntros (hd' ?); simplify_eq/=. wp_store. iApply "HΦ".
    iExists l, _, []. iFrame "Hl". by rewrite /= NoDup_nil.
  Qed.
End mset.
