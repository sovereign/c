From iris_c.vcgen Require Import proofmode.

Definition inc : val := λᶜ "l",
  c_ret "l" +=ᶜ ♯1 ;ᶜ ♯ ().

Definition factorial : val := λᶜ "n",
  "r" ←mutᶜ ♯1;ᶜ
  "c" ←mutᶜ ♯0;ᶜ
  whileᶜ (∗ᶜ (c_ret "c") <ᶜ c_ret "n") {
    callᶜ (c_ret inc) (c_ret "c");ᶜ
    c_ret "r" =ᶜ ∗ᶜ (c_ret "r") *ᶜ ∗ᶜ (c_ret "c")
  };ᶜ
  ∗ᶜ(c_ret "r").

Section factorial_spec.
  Context `{cmonadG Σ}.

  Lemma inc_spec l (n : Z) R Φ :
    l ↦C #n -∗ (l ↦C #(1 + n) -∗ Φ #()) -∗
    CWP inc (cloc_to_val l) @ R {{ Φ }}.
  Proof.
    iIntros "? H". iApply cwp_fun; simpl. vcg; iIntros "? !>". by iApply "H".
  Qed.

  Lemma factorial_spec (n: nat) R :
    CWP factorial #n @ R {{ v, ⌜v = #(fact n)⌝ }}%I.
  Proof.
    iApply cwp_fun; simpl. vcg. iIntros (r c) "**".
    iApply (cwp_wand _ (λ _, c ↦C #n ∗ r ↦C #(fact n))%I with "[-]"); last first.
    { iIntros (?) "[Hc Hr]". vcg_continue. eauto with iFrame. }
    iAssert (∃ k : nat, ⌜k ≤ n⌝ ∧ c ↦C #k ∗ r ↦C #(fact k))%I with "[-]" as (k Hk) "[??]".
    { iExists 0%nat. eauto with lia iFrame. }
    iLöb as "IH" forall (n k Hk). iApply cwp_whileV; iNext.
    vcg. iIntros "**". case_bool_decide.
    + iLeft. iSplit; eauto. iModIntro. vcg.
      iIntros "Hc Hr $ !> !>". iApply (inc_spec with "Hc"); iIntros "Hc".
      vcg_continue. iIntros "Hc Hr !>".
      assert (fact k * S k = fact (S k)) as -> by (simpl; lia).
      iApply ("IH" $! n (S k) with "[%] Hc Hr"). lia.
    + iRight. iSplit; eauto. iModIntro.
      assert (k = n) as -> by lia. by iFrame.
  Qed.
End factorial_spec.
