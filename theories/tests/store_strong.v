From stdpp Require Import namespaces.
From iris_c.vcgen Require Import proofmode.
From iris_c.lib Require Import mset list.
From iris.algebra Require Import frac_auth csum excl agree.

(** This example is meant to demonstrate the usage of atomic rules for
the primitive operations, as well as for function calls.  In this file
we verify the following program:

    #include <stdlib.h>
    #include <stdio.h>
    
    int storeme(int * l) { *l = 10; return 10; }
    
    int main() {
      int * l = calloc(sizeof(int), 1);
      int r = storeme(l) + ( *l = 11 );
      printf("*l = %d, r = %d\n", *l, r);
      return 0;
    }

We prove that the result value r is always 21 and that the value store in l
is either 10 or 11.

To prove this specification we use an invariant that tracks whether the
left/right hand side of the plus operator has been executed. This is achived
using the ghost state variables `γl` and `γr`. The invariant guarantees that
if `γl ≔ b1 ∗ γr ≔ b2`, then we have four possible situations by case-analysis.

    match b1, b2 with
    | false, false => l ↦C #0
    | true, false => l ↦C #10
    | false, true => l ↦C[LLvl] #11
    | true, true => l ↦C #10 ∨ l ↦C[LLvl] #11
    end.

*)

Definition storeme : val := λᶜ "l", c_ret "l" =ᶜ ♯ 10.

Definition test : val := λᶜ "l",
  callᶜ (c_ret storeme) (c_ret "l") +ᶜ (c_ret "l" =ᶜ ♯11).

Section test.
  Context `{cmonadG Σ, !inG Σ (authR (optionUR (exclR boolC)))}.

  (** Basic specification for `storeme' *)
  Lemma storeme_spec R cl v Φ :
    cl ↦C v -∗ (cl ↦C #10 -∗ Φ #10) -∗
    CWP storeme (cloc_to_val cl) @ R {{ Φ }}.
  Proof.
    iIntros "? H". iApply cwp_fun; simpl. vcg; iIntros "? !>". by iApply "H".
  Qed.

  (** Ghost state definitions and lemmas *)
  Definition gpointsto γ (b : bool) := own γ (◯ (Excl' b)).
  Notation "γ '≔' b" := (gpointsto γ b) (at level 80).
  Definition gauth γ b := own γ (● (Excl' b)).
  Lemma gagree γ b1 b2 :
    γ ≔ b1 -∗ gauth γ b2 -∗ ⌜b1 = b2⌝.
  Proof.
    iIntros "H1 H2".
    by iDestruct (own_valid_2 with "H2 H1")
      as %[<-%Excl_included%leibniz_equiv _]%auth_valid_discrete_2.
  Qed.
  Lemma gnew : (|==> ∃ γ, gauth γ false ∗ γ ≔ false)%I.
  Proof.
    iMod (own_alloc (● (Excl' false) ⋅ ◯ (Excl' false)))%I as (γ) "[H1 H2]";
      first done;
      eauto with iFrame.
  Qed.
  Lemma gupdate b3 γ b1 b2 :
    γ ≔ b1 -∗ gauth γ b2 ==∗ γ ≔ b3 ∗ gauth γ b3.
  Proof.
    iIntros "H1 H2".
    iMod (own_update_2 with "H2 H1") as "[? ?]".
    { apply auth_update, option_local_update.
      by apply (exclusive_local_update (Excl b2) (Excl b3)). }
    by iFrame.
  Qed.

  (** The correctness of the test function. *)
  Definition test_inv cl γl γr : iProp Σ := (∃ b1 b2, gauth γl b1 ∗ gauth γr b2 ∗
    match b1, b2 with
    | false, false => cl ↦C #0
    | true, false => cl ↦C #10
    | false, true => cl ↦C[LLvl] #11
    | _, _ => cl ↦C #10 ∨ cl ↦C[LLvl] #11
    end)%I.

  Lemma test_spec R cl `{inG Σ testR, inG Σ fracR} :
    cl ↦C #0%nat -∗
    CWP "x" ←ᶜ test (cloc_to_val cl);ᶜ c_ret "x" @ R {{ v, ⌜ v = #21 ⌝ ∧
        (cl ↦C #10 ∨ cl ↦C #11) }}.
  Proof.
    iIntros "Hl". iApply cwp_seq_bind. iApply cwp_fun. simpl.
    iMod gnew as (γl) "[H1 lb]".
    iMod gnew as (γr) "[H2 rb]".
    iApply (cwp_insert_res _ _ (test_inv cl γl γr) with "[H1 H2 Hl]").
    { iNext. iExists false,false. iFrame. }
    iApply (cwp_bin_op _ _ (λ v, ⌜v = #10⌝ ∗ γl ≔ true)%I
                           (λ v, ⌜v = #11⌝ ∗ γr ≔ true)%I
              with "[lb] [rb]").
    - vcg. unfold test_inv. iIntros "[H R]".
      iDestruct "H" as (b1 b2) "(H1 & H2 & H)".
      iDestruct (gagree with "lb H1") as %<-.
      destruct b2; iNext; iModIntro.
      + iMod (gupdate true with "lb H1") as "[lb H1]".
        iApply (storeme_spec with "H").
        iIntros "Hl". iFrame "R".
        iSplitR "lb"; last by (vcg_continue; eauto with iFrame).
        iExists _,_; eauto with iFrame.
      + iMod (gupdate true with "lb H1") as "[lb H1]".
        iApply (storeme_spec with "H").
        iIntros "Hl". iFrame "R".
        iSplitR "lb"; last by (vcg_continue; eauto with iFrame).
        iExists _,_; eauto with iFrame.
    - iApply (cwp_store _ _ (λ v, ⌜v = cloc_to_val cl⌝)%I
                            (λ v, ⌜v = #11⌝)%I).
      1,2: vcg; eauto.
      iIntros (? ? -> ->) "[H R]". unfold test_inv.
      iDestruct "H" as (b1 b2) "(H1 & H2 & H)".
      iDestruct (gagree with "rb H2") as %<-.
      iModIntro.
      destruct b1; iEval (simpl) in "H".
      + iExists cl, _. iFrame. iSplit; first done.
        iIntros "Hl".
        iMod (gupdate true with "rb H2") as "[rb H2]".
        iModIntro. iSplitR "rb"; last by eauto with iFrame.
        iExists _,_; eauto with iFrame.
      + iExists cl, _. iFrame. iSplit; first done.
        iIntros "Hl".
        iMod (gupdate true with "rb H2") as "[rb H2]".
        iModIntro. iSplitR "rb"; last by eauto with iFrame.
        iExists _,_; eauto with iFrame.
    - iIntros (v1 v2) "[% lb] [% rb]"; simplify_eq/=.
      iExists #21; simpl. iSplit; first done.
      iIntros "H". iDestruct "H" as (b1 b2) "(H1 & H2 & H)".
      do 3 iModIntro.
      iDestruct (gagree with "lb H1") as %<-.
      iDestruct (gagree with "rb H2") as %<-.
      iDestruct "H" as "[H|H]"; iModIntro; vcg; eauto with iFrame.
  Qed.
End test.

