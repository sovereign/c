(** Testing vcgen on expressions contaning unknown subexpressions *)
From iris_c.vcgen Require Import proofmode.

Section tests_vcg.
  Context `{cmonadG Σ}.

  Lemma test1 l k e :
    □ (∀ Φ v, k ↦C v -∗ (k ↦C #12 -∗ Φ v) -∗ cwp e True Φ) -∗
    l ↦C #1 -∗
    k ↦C #10 -∗
    CWP ♯ₗl =ᶜ ♯2 +ᶜ e ;ᶜ ♯ₗl =ᶜ e {{ _, True }}.
  Proof.
    iIntros "#He Hl Hk". vcg.
    iIntros "Hl Hk".
    iApply ("He" with "Hk"); iIntros "Hk".
    vcg_continue.
    iIntros "Hl Hk".
    iApply ("He" with "Hk"); iIntros "Hk".
    vcg_continue. eauto.
  Qed.

  Lemma test2 (k : cloc) :
    k ↦C #10 -∗ CWP allocᶜ (♯2,♯11) =ᶜ ∗ᶜ♯ₗk +ᶜ ♯2 {{ v, ⌜v = #12⌝ ∗ k ↦C #10}}.
  Proof.
    iIntros "Hk". vcg.
    iIntros (l) "Hl2 Hk Hl1".
    eauto 42 with iFrame.
  Qed.

  (* According to the standrad, malloc'ing zero bytes is
     implementation-defined behaviour *)
  Lemma test3 (n : nat) (m : Z) :
    1 ≤ n →
    CWP ∗ᶜ(allocᶜ (♯n,♯m)) {{ v, ⌜v = #m⌝ }}%I.
  Proof.
    intros. vcg. iExists (n). repeat iSplit; eauto.
    { iPureIntro. lia. }
    iIntros (l) "? Hl". assert (∃ m, n = S m) as [k ->]. exists (pred n). lia.
    iDestruct "Hl" as "[Hl Hls]".
    vcg_continue. eauto 42 with iFrame.
  Qed.

  Lemma test3_NOMATCH (n m : Z) :
    1 ≤ n →
    CWP ∗ᶜ(allocᶜ (♯n,♯m)) {{ v, ⌜v = #m⌝ }}%I.
  Proof.
    intros. vcg.
    match goal with
    | [ |- environments.envs_entails _
      (∃ n0 : nat, _)%I] => idtac
    end.
  Abort.

  Lemma test6 l k e1 e2 :
    (∀ Φ, Φ #11 -∗ cwp e1 True Φ) -∗
    (∀ Φ, Φ #12 -∗ cwp e2 True Φ) -∗
    l ↦C #1 -∗
    k ↦C #1 -∗
    CWP (♯ₗl =ᶜ ♯2) +ᶜ (♯ₗk =ᶜ e1;ᶜ e2)
        {{ v, l ↦C[LLvl] #2 ∗ k ↦C #11 ∧ ⌜v = #14⌝ }}.
  Proof.
    iIntros "He1 He2 Hl Hk". vcg.
    iIntros "Hk".
    iApply "He1".
    vcg_continue. iIntros "Hk".
    iApply "He2".
    vcg_continue. iIntros "Hl Hk".
    eauto with iFrame.
  Qed.
End tests_vcg.
